
var jsonLang = {
    
    inspection_report_image : {
        zh_CN : '背景图片',
        en_US : 'Inspection Report Tile Image'
    },
    home_new : {
        zh_CN : '新增',
        en_US : 'NEW'
    },
    home_report : {
        zh_CN : '檢查',
        en_US : 'Inspection Report'
    },
    menu_home : {
        zh_CN: '首頁',
        en_US: 'Home'
    },
    menu_menu : {
        zh_CN: '目录',
        en_US: 'Menu'
    },
    menu_language : {
        zh_CN: '语言',
        en_US: 'Language'
    },
    menu_settings : {
        zh_CN: '設定',
        en_US: 'Settings'
    },

    nav_prev : {
        zh_CN: '返回',
        en_US: 'Previous'
    },

    nav_next : {
        zh_CN: '下頁',
        en_US: 'Next'
    },
    nav_back : {
        en_US: 'Back',
        zh_CN: '上頁'
    },

    nav_goto : {
        zh_CN: '跳至',
        en_US: 'Go To'
    },

    nav_offline : {
        zh_CN: '离线',
        en_US: 'Offline'
    },

    nav_online : {
        zh_CN: '在线',
        en_US: 'Online'
    },

    nav_wait : {
        zh_CN: '请稍后。。',
        en_US: 'Please Wait...'
    },

    lbl_username : {
        zh_CN: '用户名',
        en_US: 'Username'
    },
    lbl_password : {
        zh_CN: '密码',
        en_US: 'Password'
    },
    lbl_defects : {
        en_US: 'Defects',
        zh_CN: '疵點'
    },
    btn_login : {
        zh_CN: '登入',
        en_US: 'Login'
    },
    btn_logout : {
        zh_CN: '登出',
        en_US: 'Logout'
    },
    btn_accept : {
        zh_CN: '接受',
        en_US: 'Accept'
    },
    btn_cancel : {
        zh_CN: '取消',
        en_US: 'Cancel'
    },
    btn_changeuser : {
        zh_CN: '切换用户',
        en_US: 'Change User'
    },
    btn_add : {
        zh_CN: '增加',
        en_US: 'Add'
    },
    btn_addcolor : {
        zh_CN: '增加颜色',
        en_US: 'Add Color'
    },
    btn_addsize : {
        zh_CN: '增加尺寸',
        en_US: 'Add Size'
    },
    btn_delete : {
        zh_CN: '删除',
        en_US: 'Delete'
    },
    btn_close : {
        zh_CN: '取消',
        en_US: 'Close'
    },
    btn_rotate : {
        zh_CN: '回转',
        en_US: 'Rotate'
    },
    //
    //  Sections
    //
    page_home : {
        en_US: 'Main Setting',
        zh_CN: '首頁設定'
    },
    page_mainsetting : {
        en_US: 'Main Setting',
        zh_CN: '首頁設定'
    },
    page_colorsize : {
        en_US: 'Color / Size',
        zh_CN: '顏色/尺碼'
    },
    page_defectpoint : {
        en_US: 'Defect Points',
        zh_CN: '疵點'
    },
    page_factorycondition : {
        en_US: 'Factory Condition',
        zh_CN: '工廠狀況'
    },
    page_measurement : {
        en_US: 'Measurement',
        zh_CN: '度尺'
    },
    page_signandsubmit : {
        en_US: 'Sign & Submit',
        zh_CN: '簽署及發送'
    },
    page_changeuser : {
        en_US: 'Change User for this Device',
        zh_CN: '切换平板用户'
    },
    page_selectpo : {
        en_US: 'Select PO',
        zh_CN: '选择订单'
    },
    page_selectfactory : {
        en_US: 'Select Factory',
        zh_CN: '选择工厂'
    },
    page_selectvendor : {
        en_US: 'Select Vendor',
        zh_CN: '选择供应商'
    },
    page_selectfirm : {
        en_US: 'Select Firm',
        zh_CN: '选择公司'
    },
    page_saving : {
        en_US: 'Saving Inspection',
        zh_CN: '存檔中'
    },

    menu_new : {
        en_US: 'New',
        zh_CN: '新增'
    },
    menu_save : {
        en_US: 'Save',
        zh_CN: '存檔'
    },
    menu_report : {
        en_US: 'Report',
        zh_CN: '報表'
    },

    lbl_inspectiontype : {
        en_US: 'Inspection Type',
        zh_CN: '查驗类别'
    },
    inlineinspection : {
        en_US: 'In-Line Inspection',
        zh_CN: '中期查驗'
    },
    finalinspection : {
        en_US: 'Final Inspection',
        zh_CN: '總期查驗'
    },
    reinspectionno : {
        en_US: 'Reinspection No',
        zh_CN: '重驗号码'
    },
    customer : {
        en_US: 'Customer',
        zh_CN: '客戶'
    },
    vendor : {
        en_US: 'Vendor',
        zh_CN: '供應商'
    },
    firm : {
        en_US: 'Company',
        zh_CN: '公司'
    },
    factory : {
        en_US: 'Factory',
        zh_CN: '工廠'
    },
    inspector : {
        en_US: 'Inspector',
        zh_CN: '檢查員'
    },
    coinspector : {
        en_US: 'Co-Inspector',
        zh_CN: '協助檢查人員'
    },
    po_no : {
        en_US: 'PO #',
        zh_CN: '訂單編號'
    },
    producttype : {
        en_US: 'Product Type',
        zh_CN: '产品种类'
    },
    style_no : {
        en_US: 'Style #',
        zh_CN: '款號'
    },
    style_desc : {
        en_US: 'Style Description',
        zh_CN: '款式'
    },
    desc : {
        en_US: 'Desc',
        zh_CN: '內容'
    },
    quantity : {
        en_US: 'Quantity',
        zh_CN: '數量'
    },
    shipdate : {
        en_US: 'Ship Date',
        zh_CN: '船期'
    },
    shipmode : {
        en_US: 'Ship Mode',
        zh_CN: '付運方式'
    },
    destination : {
        en_US: 'Destination',
        zh_CN: '目的地'
    },
    fibercontent : {
        en_US: 'Fiber Content  ',
        zh_CN: '纖維成分'
    },
    garmentweight : {
        en_US: 'Garment Weight ',
        zh_CN: '成衣重量'
    },

    color : {
        en_US: 'Color',
        zh_CN: '顏色'
    },
    size : {
        en_US: 'Size',
        zh_CN: '尺碼'
    },
    completedqty : {
        en_US: 'Completed Qty',
        zh_CN: '完成數量'
    },
    inspectedqty : {
        en_US: 'Inspected Qty',
        zh_CN: '查驗數量'
    },
    rejectedqty : {
        en_US: 'Rejected Qty',
        zh_CN: '退貨數量'
    },

    gencleanliness : {
        en_US: 'General Cleanliness',
        zh_CN: '整體整潔'
    },
    workfloorcleanliness : {
        en_US: 'Workfloor Cleanliness',
        zh_CN: '車間整潔'
    },
    workfloorligthing : {
        en_US: 'Workfloor Adequate Lighting',
        zh_CN: '車間照明足夠'
    },
    workfloorventilation : {
        en_US: 'Workfloor Ventilation',
        zh_CN: '車間通風'
    },
    childlaborawareness : {
        en_US: 'Child Labor Awareness',
        zh_CN: '發現童工'
    },
    fabricrelaxation : {
        en_US: 'Fabric Relaxation',
        zh_CN: '鬆布記錄'
    },
    brokenneedlerecord : {
        en_US: 'Broken Needle Record',
        zh_CN: '換針記錄'
    },
    no_totalworkers : {
        en_US: '# of total workers',
        zh_CN: '工人人數'
    },
    qa_accepted : {
        en_US: 'Accepted',
        zh_CN: '接受'
    },
    qa_atu : {
        en_US: 'ATU',
        zh_CN: '接受但需改善'
    },
    qa_unaccepted : {
        en_US: 'Unaccepted',
        zh_CN: '不接受'
    },

    qa_totalcompletedqty : {
        en_US: 'Total Completed Qty',
        zh_CN: '總完成數量'
    },
    qa_totalinspectedqty : {
        en_US: 'Total Inspected Qty',
        zh_CN: '總查驗數量'
    },
    qa_totalrejectedqty : {
        en_US: 'Total Rejected Qty',
        zh_CN: '總退貨數量'
    },
    qa_accepted : {
        en_US: 'Accepted',
        zh_CN: '接受'
    },
    qa_rejected : {
        en_US: 'Rejected',
        zh_CN: '拒絕'
    },
    qa_tobeconfirmed : {
        en_US: 'To be confirmed',
        zh_CN: '待定'
    },
    report_intime : {
        zh_CN: '开始时间',
        en_US: 'Start Time'
    },
    report_outtime : {
        zh_CN: '结束时间',
        en_US: 'Completed Time'
    },
    report_remark : {
        en_US: 'Remark',
        zh_CN: '備註'
    },
    report_factoryincharge : {
        en_US: 'Factory in-charge',
        zh_CN: '廠方負責人'
    },
    report_fullname : {
        en_US: 'Factory In-Charge Name',
        zh_CN: '廠方負責人名称'
    },
    report_title : {
        en_US: 'Factory In-Charge Title',
        zh_CN: '廠方負責人职位'
    },
    report_email : {
        en_US: 'Factory Contact Email/s',
        zh_CN: '廠方負責人电邮'
    },
    action_submit : {
        en_US: 'Submit',
        zh_CN: '發送'
    },
    inspector : {
        en_US: 'Inspector',
        zh_CN: '檢查員簽名'
    },
    manufacturer : {
        en_US: 'Manufacturer',
        zh_CN: '工廠代表簽名'
    },
    manager : {
        en_US: 'Q.A. Manager',
        zh_CN: 'Q.A.經理簽名'
    },
    merchandizingmanager : {
        en_US: 'Merchandising Manager',
        zh_CN: '營業部經理簽名'
    },

    //defect points
    header_defect_point : {
        en_US: 'Defect Point',
        zh_CN: '疵点'
    },
    select_color_n_size : {
        en_US: 'Select Color &amp, Size',
        zh_CN: '選擇顏色尺碼'
    },
    completed_qty : {
        en_US: 'Completed Quantity',
        zh_CN: '完成數量'
    },
    inspected_qty : {
        en_US: 'Inspected Quantity',
        zh_CN: '查驗數量'
    },
    rejected_qty : {
        en_US: 'Rejected Quantity',
        zh_CN: '退貨數量'
    },
    select_defect_cat : {
        en_US: 'Select Defect Category',
        zh_CN: '選擇疪點類別'
    },
    remarks_reason : {
        en_US: 'Remarks / Reason',
        zh_CN: '備註/理由'
    },
    label : {
        en_US: 'Label',
        zh_CN: '名称'
    },
    capture_photo : {
        en_US: '拍照',
        zh_CN: 'Capture Photo'
    },
    browse_photo : {
        en_US: 'Browse Photo',
        zh_CN: '選擇照片'
    },
    add_new_row : {
        en_US: 'Add New Row',
        zh_CN: '新增行'
    },
    gototop : {
        en_US: 'Goto Top',
        zh_CN: '返回最上面'
    },
    clear_all_fields : {
        en_US: 'Clear All Fields',
        zh_CN: '从新输入'
    },

    //Home
    new_inspection : {
        en_US: 'New Inspection',
        zh_CN: '新增查驗報告'
    },

    //Index
    install : {
        en_US: 'Install',
        zh_CN: '安裝'
    },

    //Main
    header_main_settings : {
        en_US: 'Main Settings',
        zh_CN: '主要设定'
    },
    header_inspection_info : {
        en_US: 'Inspection Info',
        zh_CN: '查驗讯息'
    },
    header_po_info : {
        en_US: 'PO Info',
        zh_CN: 'PO 讯息'
    },
    header_color_size_settings : {
        en_US: 'Color / Size Settings',
        zh_CN: '颜色 / 尺寸设定'
    },
    trim : {
        en_US: 'Trim',
        zh_CN: '物料'
    },
    result : {
        en_US: 'Result',
        zh_CN: '結果'
    },
    add_color : {
        en_US: 'Add Color',
        zh_CN: '新增顏色'
    },
    update_color : {
        en_US: 'Update Color',
        zh_CN: '更改颜色'
    },
    import_colorsize : {
        en_US: 'Import Color / Size',
        zh_CN: '导入顏色 / 尺碼'
    },
    delete_color : {
        en_US: 'Delete Color',
        zh_CN: '删除颜色'
    },
    total : {
        en_US: 'Total',
        zh_CN: '总数'
    },
    saved_color : {
        en_US: 'Saved Color',
        zh_CN: '已存档颜色'
    },
    add_size : {
        en_US: 'Add Size',
        zh_CN: '新增尺碼'
    },
    update_size : {
        en_US: 'Update Size',
        zh_CN: '更新尺碼'
    },
    delete_size : {
        en_US: 'Delete Size',
        zh_CN: '删除尺碼'
    },
    saved_size : {
        en_US: 'Saved Size',
        zh_CN: '已存档尺碼'
    },
    capture : {
        en_US: 'Capture',
        zh_CN: '撷取'
    },
    browse : {
        en_US: 'Browse',
        zh_CN: '浏览'
    },
    date : {
        en_US: 'Date',
        zh_CN: '日期'
    },
    select : {
        en_US: 'Status',
        zh_CN: '狀態'
    },
    warning : {
        en_US: 'Warning',
        zh_CN: '警告'
    },
    fields_missed : {
        en_US: 'There are some fields you missed.',
        zh_CN: '還有資料未被輸入'
    },
    btn_ok : {
        en_US: 'Ok',
        zh_CN: '確定'
    },
    header_selectserver: {
        en_US: 'Select Server URL',
        zh_CN: '选择服务器路径'
    },
    header_serverurl : {
        en_US: 'Server URL',
        zh_CN: '服务器路径'
    },

    // SAMPLE
    inspection : {
        en_US: 'Inspection',
        zh_CN: '查驗'
    },
    reinspect : {
        en_US: 'Reinspect #',
        zh_CN: '重新查驗次數'
    },
    ponumber : {
        en_US: 'P. O. Number',
        zh_CN: '訂單編號'
    },
    description : {
        en_US: 'Description',
        zh_CN: '內容'
    },
    style : {
        en_US: 'Style #',
        zh_CN: '款號'
    },
    select_size : {
        en_US: 'Select Size',
        zh_CN: '選擇尺碼'
    },
    measurement_qty : {
        en_US: 'Measurement Quantity',
        zh_CN: '度尺數量'
    },
    total_inspected_qty : {
        en_US: 'Total Inspected Quantity',
        zh_CN: '總查驗數量'
    },
    total_completed_qty : {
        en_US: 'Total Completed Quantity',
        zh_CN: '總完成數量'
    },
    intime : {
        en_US: 'In-Time',
        zh_CN: '到廠時間'
    },
    outtime : {
        en_US: 'Out-Time',
        zh_CN: '離廠時間'
    },
    remarks : {
        en_US: 'Remarks',
        zh_CN: '備註'
    },
    //SIGNING
    clear : {
        en_US: 'Clear',
        zh_CN: '重新输入'
    },
    //VIEW
    factory_qc_name : {
        en_US: 'Factory QC Name',
        zh_CN: '廠方 QC'
    },
    factory_in_charge_name : {
        en_US: 'Factory In-Charge Name',
        zh_CN: '廠方負責人'
    },
    factory_in_charge_position : {
        en_US: 'Factory In-Charge Position',
        zh_CN: '廠方負責人職位'
    },
    inspection_no : {
        en_US: 'Inspection #',
        zh_CN: '查驗報告編號'
    },
    status : {
        en_US: 'Status',
        zh_CN: '狀況'
    },
    total_completed_city : {
        en_US: 'Total Completed Qty',
        zh_CN: '總完成數量'
    },
    total_inspected_city : {
        en_US: 'Total Inspected Qty',
        zh_CN: '總查驗數量'
    },
    total_rejected_city : {
        en_US: 'Total Rejected Qty',
        zh_CN: '總退貨數量'
    },
    manufacturer_signature : {
        en_US: "Manufacturer's Signature",
        zh_CN: "工廠代表簽名"
    },
    inspector_signature : {
        en_US: "Inspector's Signature",
        zh_CN: "验货员簽名"
    },
    sign_this_report : {
        en_US: 'Sign This Report',
        zh_CN: '查驗報告加簽'
    },
    change_update : {
        en_US: 'Change / Update',
        zh_CN: '修改/更新'
    },
    copy_report : {
        en_US: 'Copy Report',
        zh_CN: '拷貝查驗報告'
    },
    delete_this_report : {
        en_US: 'Delete This Report',
        zh_CN: '刪除查驗報告'
    },
    delete_inspection : {
        en_US: 'Delete Inspection?',
        zh_CN: '删除查驗報告?'
    },
    are_you_sure_delete : {
        en_US: 'Are you sure you want to delete this Inspection?',
        zh_CN: '是否確定刪除此查驗報告?'
    },
    cannot_undone : {
        en_US: 'This action cannot be undone.',
        zh_CN: '这动作不能还原.'
    },
    signpage_question : {
        en_US: 'Sign and Submit?',
        zh_CN: '签名并提交?'
    },
    btn_no : {
        en_US: 'No',
        zh_CN: '否'
    },
    btn_yes : {
        en_US: 'Yes',
        zh_CN: '是'
    },
    areyousure_delete_photo : {
        en_US: 'Are you sure you want to delete this photo?',
        zh_CN: '是否确定刪除此照片?'
    },
    confirm_delete_photo : {
        en_US: 'Confirm delete  photo?',
        zh_CN: '确定刪除照片'
    },
    cancel_signing : {
        en_US: 'Are you sure you want to cancel?',
        zh_CN: '是否确定取消?'
    },
    report_email_settings : {
        en_US: 'Report Email Settings',
        zh_CN: '报表电邮设定'
    },
    report_email_recipient : {
        en_US: 'Email Group',
        zh_CN: '电邮群组'
    },
    accept : {
        en_US: 'Accept',
        zh_CN: '接受'
    },
    reject : {
        en_US: 'Reject',
        zh_CN: '不接受'
    },
    status_default : {
        en_US: 'Please select status',
        zh_CN: '请选择状态'
    },
    status_accepted : {
        en_US: 'Accept',
        zh_CN: '接受'
    },
    status_reinspection : {
        en_US: 'Re-Inspection',
        zh_CN: '重新查驗'
    },
    status_hold_shipment : {
        en_US: 'Hold Shipment',
        zh_CN: '暂停发货'
    },

    qc_comment : {
        en_US: 'QC Comment',
        zh_CN: 'QC 注释'
    },

    qc_remark : {
        en_US: 'QC Remarks',
        zh_CN: 'QC 备注'
    },

    btn_new : {
        en_US: 'New PO',
        zh_CN: 'New PO'
    },
    supervisor_email : {
        en_US: 'Supervisor Email(s)',
        zh_CN: '主管电邮'
    },

    // dialogs
    dlg_loginsuccess : {
        en_US: 'Login Successful',
        zh_CN: '登录成功'
    },
    dlg_loginfail : {
        en_US: 'Invalid username/password',
        zh_CN: '用户名或密码错误'
    },
    dlg_enteradminpwd : {
        en_US: 'Please enter administrator password:',
        zh_CN: '请输入管理员密码：'
    },
    dlg_adminrights : {
        en_US: 'Security Password:',
        zh_CN: '管理员权限：'
    },
    dlg_appversion : {
        en_US: 'MagQA Version',
        zh_CN: 'MagQA版本'
    },
    dlg_admindenied : {
        en_US: 'Incorrect Admin Password',
        zh_CN: '管理员密码错误'
    },


    // Report Headers
    lbl_ship_qty : {
        en_US: 'Shipment / Qty',
        zh_CN: '送貨數量'
    },
    lbl_email : {
        en_US: 'Email',
        zh_CN: '电邮'
    },
    lbl_purchaseorder : {
        en_US: 'Purchase Order',
        zh_CN: '订单'
    },
    lbl_colorsize : {
        en_US: 'Color/Size',
        zh_CN: '顏色/尺碼'
    },
    lbl_defect_qty : {
        en_US: 'Defect Qty',
        zh_CN: '疵點數量'
    },
    lbl_image_qty : {
        en_US: '# of Images',
        zh_CN: '图片數量'
    },
    lbl_refno : {
        en_US: 'Ref#',
        zh_CN: '参考号'
    },
    lbl_condition : {
        en_US: 'Condition',
        zh_CN: '狀況'
    },
    lbl_signature : {
        en_US: "Signature",
        zh_CN: "簽名"
    },
    lbl_photo : {
        en_US: 'Photo',
        zh_CN: '照片'
    },
    lbl_addphoto : {
        en_US: 'Add Photo',
        zh_CN: '增加照片'
    },
    btn_updatedb : {
        en_US: 'Update DB',
        zh_CN: '更新数据库'
    },
    lbl_major : {
        en_US: 'Major',
        zh_CN: '主要'
    },
    lbl_minor : {
        en_US: 'Minor',
        zh_CN: '次要'
    },
    nophoto : {
        en_US: 'no photo',
        zh_CN: '没有照片'
    },
    hasnophoto : {
        en_US: 'has no photo',
        zh_CN: '没有照片'
    },
    short_po_qty : {
        en_US: 'PO Qty',
        zh_CN: '订单數量'
    },
    short_completed_qty : {
        en_US: 'Completed Qty',
        zh_CN: '完成數量'
    },
    short_inspected_qty : {
        en_US: 'Inspected Qty',
        zh_CN: '查驗數量'
    },
    short_rejected_qty : {
        en_US: 'Rejected Qty',
        zh_CN: '退貨數量'
    },
    notfound : {
        en_US: 'Not Found',
        zh_CN: '没有相关记录'
    },
    isrequired : {
        en_US: 'is required',
        zh_CN: '不能缺省'
    },
    signaturesaved : {
        en_US: 'Signature is saved',
        zh_CN: '签名已存档'
    },

    retryonline : {
        zh_CN: '离线-请连网后重试',
        en_US: 'Offline - Please connect and try again'
    },

    cannotimportcolorsize : {
        en_US: 'Import Color / Size Fail',
        zh_CN: '导入顏色 / 尺碼失败'
    },
    cannotfindinspectionno : {
        en_US: 'Inspection # not yet submitted',
        zh_CN: '查驗報告編號还没有提交'
    },
    noinspectionno : {
        en_US: 'Inspection # cannot be blank',
        zh_CN: '查驗報告編號不能缺省'
    },

    searchpo : {
        zh_CN: '寻找 PO : ',
        en_US: 'Search PO : '
    },

    selectedpolist : {
        zh_CN: '已选 PO 列表 : ',
        en_US: 'Selected PO List : '
    },

    selectedpoqty : {
        zh_CN: '已选 PO 送货数量 : ',
        en_US: 'Selected PO Qty : '
    }
};