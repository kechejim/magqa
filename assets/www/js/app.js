
var App = {
	ws_url : localStorage.getItem("ws_url"),
	version : '2014-10-16-1',
	current : 'start',
	isOnline : false,
	page : {}
};

//for start
App.page.start = {};
App.page.start.init = function(){
	location.href = "login.html#updatedb";
};

//for main
App.page.main = {};

App.page.main.showStatus = function(){
	var po = cloneOmitObject(App.page.main.POData);
	var ins = cloneOmitObject(App.page.main.InspectionData);

	//console.log(JSON.stringify(po));
	//console.log(JSON.stringify(ins));
}

App.page.main.POData = {
	id: null,
	po_no: null,
	firm:null,
	vendor: null,
	customer: null,
	qty: null,
	shipdate: null,
	shipmode: null,
	factory: null,
	syncstatus: null,
	style: null,
	destination:null,
	producttype:null,
	styledesc:null,
	factory: null,
	fibercontent: null,
	garmentweight: null,
	description: null,
	//colorsize : {colors:[],size:[]}
};
// Bernard
App.page.main.InspectionData = {
	id: null,
	po_no: null,
	//inspection_serial_id: null,
	inspection_date: getCurrentDate(),
	inspector: null,
	coinspector: null,
	//firm : null,
	//vendor : null,
	//customer: null,
	status: 0,
	reinspect_count: null,
	//factory: null,
	inspectiontype: null,
	//colorsize_defect_points: null,
	//colorsize_POMs: null,
	general_cleanliness: {"photos":[],"status":"1"},
	workflow_cleanliness: {"photos":[],"status":"1"},
	workflow_lighting: {"photos":[],"status":"1"},
	workflow_ventilation: {"photos":[],"status":"1"},
	child_labor: {"photos":[],"status":"1"},
	fabric_relaxation: {"photos":[],"status":"1"},
	broken_needle_record: {"photos":[],"status":"1"},
	colorsize : {colors:[],size:[]},
	//totalnumworkers: null,
	//totalcompletedqty: null,
	//totalinspectedqty: null,
	//totalrejectedqty: null,
	//intime: null,
	//outtime: null,
	//remarks: null,
	//inspection_status: null,
	factory_photos: [],
	//factory_qc_name: null,
	//factory_contacts_fullname: null,
	//factory_contacts_title: null,
	//factory_contacts_emailaddress: null,
	//report_email_recipient: null,
	//supervisor_email: null,
	date_created: getCurrentDateTime(),
	date_modified: getCurrentDateTime()
	//inspector_sign: null,
	//co_inspector_sign: null,
	//qc_status: null,
	//qc_remarks: null,
	//factory_sign: null
};

//App.page.main.showStatus();

App.page.main.init = function(){	
	console.log("App.page.main.init");
	localization.locale = Storage.get('locale');
	localization.initialize();
	
	try{
		App.page.main.InspectionData.id = location.href.split('#')[1];
		console.log("Setting App.page.main.InspectionData.id to " + App.page.main.InspectionData.id);
		if(location.href.split('#')[2]=='copy'){
			App.page.main.copy = true;			
		}
	}catch(e){
		App.page.main.InspectionData.id = null;
	}

	App.page.main.initMain();

	$(document).on('pageshow', '#colorsize', function(e){
		App.page.main.initColorSize();
	});

	$(document).on('pageshow', '#defectpoints', function(e){
		App.page.main.initDefectPoints();
	});

	$(document).on('pageshow', '#factorycondition', function(e){
		App.page.main.initFactory();
	});

	$(document).on('pageshow', '#measurement', function(e){
		App.page.main.initMeasurements();
	});

	$(document).on('pageshow', '#trim', function(e){
		App.page.main.initTrim();
	});

	$(document).on('pageshow', '#result', function(e){
		App.page.main.initResult();
	});

	$(document).on('pageshow', '#selectpo', function(e){
		App.page.main.initSelectPO();
	});

	$(document).on('pageshow', '#selectvendor', function(e){
		App.page.main.initSelectVendor();
	});

	$(document).on('pageshow', '#selectfactory', function(e){
		App.page.main.initSelectFactory();
	});

	$(document).on('click', '.link-home', function(e){
		e.preventDefault();
		App.page.main.save(false,true);
//		document.location = 'home.html';
//		setTimeout(function(){
//			location.href = 'home.html';
//		},1000);
		
	});

	$(document).on('click', '.link-settings', function(e){
		//App.page.main.save();
		e.preventDefault();
	});	
	
	$("input").on('input', function() { 
		App.page.main.save();
	});
	$("textarea").on('input', function() { 
		App.page.main.save();
	});
};

//Dave
App.page.main.initInspectionDetailForCopy = function(){
	
	console.log("init Inspection Detail for Copy Report");
	var user = Storage.get('user-login');
	
	var populateInspectionFields = function(){
		$.each(App.page.main.InspectionData, function(k,v){
			//console.log("  - Updating " + k + " = " + v);
			$('#po-text-'+k).val(v);
			$('#po-select-'+k).val(v);
			$('#text-'+k).val(v);
			$('#select-'+k).val(v);
		});
	}

	var select = ['inspections.*'];
	var sql = 'inspections WHERE inspections.id = ' + App.page.main.InspectionData.id + '';

	DB.find(sql, select, function(err, results) {
		console.log("Got Inspections");
		item = results.rows.item(0);

		if(typeof(results) != 'undefined' && results.rows){
			App.page.main.InspectionData = {};
			var items = results.rows.item(0);
			var json_objects = {
				'general_cleanliness':'general_cleanliness',
				'workflow_cleanliness':'workflow_cleanliness',
				'workflow_ventilation':'workflow_ventilation',
				'workflow_lighting':'workflow_lighting',
				'child_labor':'child_labor',
				'fabric_relaxation':'fabric_relaxation',
				'broken_needle_record':'broken_needle_record',
				'colorsize_POMs':'colorsize_POMs',
				'colorsize':'colorsize',
				"colorsize_defect_points": "colorsize_defect_points",
				"factory_photos": "factory_photos",
				"trim": "trim"
			};

			$.each(App.page.main.InspectionData, function(k, l){		
				if(fields[k]!=undefined){
					App.page.main.InspectionData[k] = fields[v];
				}
			});

			$.each(items, function(k,v){
				//console.log("processing : "+k +":" + unescape(items[k]));

				if(json_objects[k]==k){

					if (k == 'factory_photos') {
						try{
							App.page.main.InspectionData[k] = $.parseJSON(unescape(items[k]));
						}catch(e){
							App.page.main.InspectionData[k] = [];
						}				
						if(check(App.page.main.InspectionData[k])){
							App.page.main.InspectionData[k] = [];
						}	
					} else {
						try{
							App.page.main.InspectionData[k] = $.parseJSON(unescape(items[k]));
						}catch(e){
							App.page.main.InspectionData[k] = {};
						}				
						if(check(App.page.main.InspectionData[k])){
							App.page.main.InspectionData[k] = {};
						}	
					}
				}else{
					App.page.main.InspectionData[k] = items[k];
				}					
			});

			// initialize fields in the case of copying inspectiondata

			App.page.main.InspectionData.id = null;
			App.page.main.InspectionData.inspection_serial_id = null;
				

			//defect points
			App.page.main.InspectionData.colorsize_defect_points = {};

			//factory condition
			App.page.main.InspectionData['general_cleanliness'] = {"photos":[],"status":"1"};
			App.page.main.InspectionData['workflow_cleanliness'] = {"photos":[],"status":"1"};
			App.page.main.InspectionData['workflow_ventilation'] = {"photos":[],"status":"1"};
			App.page.main.InspectionData['workflow_lighting'] = {"photos":[],"status":"1"};
			App.page.main.InspectionData['child_labor'] = {"photos":[],"status":"1"};
			App.page.main.InspectionData['fabric_relaxation'] = {"photos":[],"status":"1"};
			App.page.main.InspectionData['broken_needle_record'] = {"photos":[],"status":"1"};
			App.page.main.InspectionData['child_labor'] = {"photos":[],"status":"1"};
			App.page.main.InspectionData.totalnumworkers = 0;
			
			//measurement photos
			App.page.main.InspectionData.factory_photos = {};

			//Trim
			App.page.main.InspectionData.trim ={};		
			
			App.page.main.InspectionData.coinspector = "";
			App.page.main.InspectionData.reinspect_count += 1;
			App.page.main.InspectionData.intime = "";
			App.page.main.InspectionData.outtime = "";
			App.page.main.InspectionData.inspection_status = "";
			App.page.main.InspectionData.qc_status = "";
			App.page.main.InspectionData.qc_remarks = "";
			App.page.main.InspectionData.status = 0;
			delete(App.page.main.InspectionData.inspector_sign);
			delete(App.page.main.InspectionData.factory_sign);


			populateInspectionFields();
			
			var sql = 'customers where firm like "%'+$('#select-firm').val()+'%" '
			var fields = ['customers.*'];

			DB.find(sql,fields,function(err, results){
				if (results) {
					if (results.rows) {
						var sel = [];
						for(i=0;i<=results.rows.length-1;i++){
							var fields = results.rows.item(i);
							sel.push('<option value="'+fields.code+'">'+fields.name+'</option>');
						}
						$('#select-customer').empty().append(sel);
						$('#select-customer').val(App.page.main.InspectionData['customer']);
						$('#select-customer').selectmenu("refresh");		
					}
				}
			});

			sql = 'producttypes where firm like "%'+$('#select-firm').val()+'%" '
			fields = ['producttypes.*'];

			DB.find(sql,fields,function(err, results){
				//console.log("rows=" + results.rows.length);
				
				if (results) {
					if (results.rows) {
						var sel = [];
						for(i=0;i<=results.rows.length-1;i++){
							var fields = results.rows.item(i);
							sel.push('<option value="'+fields.code+'">'+fields.name+'</option>');
						}
						$('#select-producttype').empty().append(sel);
						$('#select-producttype').val(App.page.main.InspectionData['producttype']);
						$('#select-producttype').selectmenu("refresh");	
					}
				}
			});
			
			try{
				var fsign = unescape(App.page.main.InspectionData.tileimage);
				if(fsign === "undefined"){
					$('#select_inspection_report_image').attr('src', 'images/icon-shirt.png');	
				} else {
					$('#select_inspection_report_image').attr('src', fsign);
				}
			}catch(e){
				alert(e);
			}
			//$('#select_inspection_report_image').attr('src', App.page.main.InspectionData.tileimage);

			$('#select-firm').selectmenu("refresh");
			$('#select-customer').selectmenu("refresh");
			$('#select-producttype').selectmenu("refresh");
			$('#select-report_email_recipient').selectmenu("refresh");					
			
		} else {
			console.log("Error retrieving inspection");
			return;
		}
		console.log("Retrieve Inspection Finished");
		//retrievePO();
	});

};

//Bernard
App.page.main.getInspectionDetail = function(){
	
	console.log("Get Inspection Detail");
	var user = Storage.get('user-login');
		
	var populateInspectionFields = function(){
		$.each(App.page.main.InspectionData, function(k,v){
			//console.log("  - Updating " + k + " = " + v);
			$('#po-text-'+k).val(v);
			$('#po-select-'+k).val(v);
			$('#text-'+k).val(v);
			$('#select-'+k).val(v);
		});
	}	

	var populatePhotos = function() {
		console.log("Load Factory Photos");
		$('.div-factory').each(function(){
			console.log("Find .div-factory");
			var id = $(this).attr('id');
			var container = $(this);
			var photo_container = container.find('.photos');
			//container.find('.remarks').hide();
			var block = ['a','b','c'];
			// console.log(App.page.main.InspectionData[id]);
			photo_container.html('');
			if(App.page.main.InspectionData[id]!=null){
				//container.find('select.factory-status').val(App.page.main.InspectionData[id].status).selectmenu('refresh');
				container.find('textarea').val(App.page.main.InspectionData[id].remarks);
				// alert(App.page.main.InspectionData[id].status);
				if(App.page.main.InspectionData[id].status==2){
					container.find('.remarks').show();
				}
				try{
					var photoNo=0;
					$.each(App.page.main.InspectionData[id].photos, function(k,v){	
						photoNo++;	
						// container.html(v);
						photo_container.append('<div class="ui-block-'+block[k]+'"><div class="ui-bar"><img id = "'+id+'-'+photoNo+'" src="data:image/png;base64,'+ v +'"> </div></div>');
					});
				}catch(e){
				}
			}
			
			container.find('select.factory-status').off('change').on('change', function(e){
				if($(this).val()==2){
					container.find('.remarks').show();
				} else {
					container.find('.remarks').hide();				
				}
				App.page.main.InspectionData[id].status = container.find('select.factory-status').val();						
				$(this).selectmenu('refresh');
			});
			container.find('textarea').off('change').on('change', function(e){
				App.page.main.InspectionData[id].remarks = $(this).val();						
			});
		});
	}
/*
	var retrievePO = function() {
		console.log("retrievePO");
		if($.trim(App.page.main.InspectionData.po_no)!=""){
			console.log("App.page.main.InspectionData.po_no:" + App.page.main.InspectionData.po_no);
			var sql = "po where cast(po_no as text)= '" + App.page.main.InspectionData.po_no+"'";
			var fields = ['po.*'];
			console.log(sql);
			DB.find(sql,fields,function(e, r){
				if(e){ 
					console.log(e);
				}
				if(typeof(r) != 'undefined' && r.rows){
					if(r.rows.length>0){
						var fields = clone(r.rows.item(0));
						$.each(App.page.main.POData, function(po_key, po_val){
							if(fields[po_key]!=undefined){
								App.page.main.POData[po_key] = fields[po_key];
							}
						});
						$.each(App.page.main.POData, function(k,v){
							console.log("  - Updating "+ k + " = " + v);
							$('#po-text-'+k).val(v);
							$('#po-select-'+k).val(v);
							$('#text-'+k).val(v);
							$('#select-'+k).val(v);
						});

						try{
							App.page.main.InspectionData.colorsize = $.parseJSON(unescape(App.page.main.InspectionData.colorsize));
						}catch(e){
							App.page.main.InspectionData.colorsize = {};
						}
						if(check(App.page.main.InspectionData.colorsize)){
							App.page.main.InspectionData.colorsize = {};
						}
						console.log(App.page.main.POData);
						//populatePOFields();
						populatePhotos();

						console.log("select-firm");
						console.log($('#select-firm').val());
						$('#select-firm').selectmenu("refresh");

					}

				}
				if($('#select-qc_status').val()==2){
					$('.qc-remarks').show();
				} else {
					$('.qc-remarks').hide();
				}
				console.log("retrievePO Finished");
				App.page.main.showStatus();
			});

		};
	};
*/	
	var select = ['inspections.*'];

	var sql = 'inspections WHERE inspections.id = ' + App.page.main.InspectionData.id + '';

	DB.find(sql, select, function(err, results) {
		console.log("Got Inspections");
		item = results.rows.item(0);

		if(typeof(results) != 'undefined' && results.rows){
			App.page.main.InspectionData = {};
			var items = results.rows.item(0);
			var json_objects = {
				'general_cleanliness':'general_cleanliness',
				'workflow_cleanliness':'workflow_cleanliness',
				'workflow_ventilation':'workflow_ventilation',
				'workflow_lighting':'workflow_lighting',
				'child_labor':'child_labor',
				'fabric_relaxation':'fabric_relaxation',
				'broken_needle_record':'broken_needle_record',
				'colorsize_POMs':'colorsize_POMs',
				'colorsize':'colorsize',
				"colorsize_defect_points": "colorsize_defect_points",
				"factory_photos": "factory_photos",
				"trim": "trim"
			};

			$.each(App.page.main.InspectionData, function(k, l){		
				if(fields[k]!=undefined){
					App.page.main.InspectionData[k] = fields[v];
				}
			});

			$.each(items, function(k,v){
				//console.log("processing : "+k +":" + unescape(items[k]));

				if(json_objects[k]==k){

					if (k == 'factory_photos') {
						try{
							App.page.main.InspectionData[k] = $.parseJSON(unescape(items[k]));
						}catch(e){
							App.page.main.InspectionData[k] = [];
						}				
						if(check(App.page.main.InspectionData[k])){
							App.page.main.InspectionData[k] = [];
						}	
					} else {
						try{
							App.page.main.InspectionData[k] = $.parseJSON(unescape(items[k]));
						}catch(e){
							App.page.main.InspectionData[k] = {};
						}				
						if(check(App.page.main.InspectionData[k])){
							App.page.main.InspectionData[k] = {};
						}	
					}
				}else{
					App.page.main.InspectionData[k] = items[k];
				}					
			});
			
			populateInspectionFields();
			
			var sql = 'customers where firm like "%'+$('#select-firm').val()+'%" '
			var fields = ['customers.*'];

			DB.find(sql,fields,function(err, results){
				if (results) {
					if (results.rows) {
						var sel = [];
						for(i=0;i<=results.rows.length-1;i++){
							var fields = results.rows.item(i);
							sel.push('<option value="'+fields.code+'">'+fields.name+'</option>');
						}
						$('#select-customer').empty().append(sel);
						$('#select-customer').val(App.page.main.InspectionData['customer']);
						$('#select-customer').selectmenu("refresh");		
					}
				}
			});

			sql = 'producttypes where firm like "%'+$('#select-firm').val()+'%" '
			fields = ['producttypes.*'];

			DB.find(sql,fields,function(err, results){
				//console.log("rows=" + results.rows.length);
				
				if (results) {
					if (results.rows) {
						var sel = [];
						for(i=0;i<=results.rows.length-1;i++){
							var fields = results.rows.item(i);
							sel.push('<option value="'+fields.code+'">'+fields.name+'</option>');
						}
						$('#select-producttype').empty().append(sel);
						$('#select-producttype').val(App.page.main.InspectionData['producttype']);
						$('#select-producttype').selectmenu("refresh");	
					}
				}
			});
			try{
				var fsign = unescape(App.page.main.InspectionData.tileimage);
				if(fsign === "undefined"){
					$('#select_inspection_report_image').attr('src', 'images/icon-shirt.png');	
				} else {
					$('#select_inspection_report_image').attr('src', fsign);
				}
			}catch(e){
				alert(e);
			}		
			//$('#select_inspection_report_image').attr('src', App.page.main.InspectionData.tileimage);
									
			$('#select-firm').selectmenu("refresh");
			$('#select-customer').selectmenu("refresh");
			$('#select-producttype').selectmenu("refresh");
			$('#select-report_email_recipient').selectmenu("refresh");		
			
		} else {
			console.log("Error retrieving inspection");
			return;
		}
		console.log("Retrieve Inspection Finished");
		//retrievePO();

	});

	$('#select-qc_status').off('change').on('change', function(e){
		if($(this).val()==2){
			$('.qc-remarks').show();
		} else {
			$('.qc-remarks').hide();
		}
	});
};

App.page.main.validateMain = function(){
	var result = true;
	var po_number = $.trim($('#po-text-po_no').val());
	if( po_number.length == '' ){
		result = false;
		$('label[for="po-text-po_no"] > span').html(localization.get('po_no') + ' '+localization.get('isrequired')+'').css({color:'#cc0000'});
	} else {
		$('label[for="po-text-po_no"] > span').html(localization.get('po_no')).css({color:''});
	}
	
	var product_type = number($('#po-select-producttype_id').val());
	if(number(product_type) == 0){
		result = false;
		$('label[for="po-select-producttype_id"] > span').html(localization.get('producttype') + ' '+localization.get('isrequired')+'').css({color:'#cc0000'});
	} else {
		$('label[for="po-select-producttype_id"] > span').html(localization.get('producttype')).css({color:''});
	}
	return result;
};

App.page.main.initMain = function(){

	console.log("Init Main");

	getGPSLocation(function(Loc) {

	/*	'Latitude: '           : position.coords.latitude  
        'Longitude: '          : position.coords.longitude 
        'Altitude: '           : position.coords.altitude  
        'Accuracy: '           : position.coords.accuracy  
        'Altitude Accuracy: '  : position.coords.altitudeAccuracy
        'Heading: '            : position.coords.heading         
        'Speed: '              : position.coords.speed           
        'Timestamp: '          : new Date(position.timestamp)    */  
     
		App.page.main.InspectionData.GPSLongitude = Loc.coords.longitude;
		App.page.main.InspectionData.GPSLatitude = Loc.coords.latitude;

		console.log('Location : ' + Loc.coords.longitude + ',' + Loc.coords.latitude);
	});

	$('.tileimage-capture,.tileimage-browse').off('click').on('click', function(e){
		e.preventDefault();

		if($(this).hasClass('tileimage-browse')){
			getPhoto(Camera.PictureSourceType.PHOTOLIBRARY, function(imgSource){
				try{
					$('#select_inspection_report_image').attr('src', 'data:image/png;base64,'+ imgSource);
					App.page.main.InspectionData.tileimage = 'data:image/png;base64,' + imgSource;					
				}catch(e){					
				}
			});
		}else{
			captureImage(function(imgSource){
				try{
					$('#select_inspection_report_image').attr('src', 'data:image/png;base64,'+ imgSource);									
					App.page.main.InspectionData.tileimage = 'data:image/png;base64,' + imgSource;
				}catch(e){
				}
			});
		}
		
	});

	$('#select_inspection_report_image').off('click').on('click', function(e){
		
		rotateBase64Image (App.page.main.InspectionData.tileimage, function(rotatedData){
			
			App.page.main.InspectionData.tileimage = rotatedData;

			$('#select_inspection_report_image').attr('src', App.page.main.InspectionData.tileimage);
					
			App.page.main.save()
		});	
		
	});

	$('.validateMain').off('click').on('click', function(e){
		if(App.page.main.validateMain()){
			return true;
		} else {
			e.preventDefault();
			return false;
		}
	});
	
	$('.lang-en').off('click').on('click', function(e){
		Storage.set('locale', 'en_US');
		localization.locale = 'en_US';
		localization.initialize();
	});
	
	$('.lang-ch').off('click').on('click', function(e){
		Storage.set('locale', 'zh_CN');
		localization.locale = 'zh_CN';
		localization.initialize();
	});
	
	$('#link-main-home-back').off('click').on('click', function(e){
		App.page.main.save(false,true);
//		document.location = 'home.html';
//		setTimeout(function(){
//			location.href = 'home.html';
//		},1000);
	});

	$('#po-text-po_no').off('click').on('click', function(){
		$.mobile.changePage('#selectpo', {
			transition : 'slide'
		});
	});
	$('#po-text-vendor').off('click').on('click', function(){
		$.mobile.changePage('#selectvendor', {
			transition : 'slide'
		});
	});
	$('#po-text-factory').off('click').on('click', function(){
		$.mobile.changePage('#selectfactory', {
			transition : 'slide'
		});
	});

	var loadFirms = function(){
		var sql = 'firm left join linkfirminspectors on firm.code=linkfirminspectors.firmcode '
			sql += 'where userid="'+user.username+'"';
		var fields = ['firm.*'];

		//console.log(sql);
		DB.find(sql,fields,function(err, results){
			//console.log("rows=" + results.rows.length);
			var sel = [];
			sel.push('<option value="">NONE</option>');
			for(i=0;i<=results.rows.length-1;i++){
				var fields = results.rows.item(i);
				sel.push('<option value="'+fields.code+'">'+fields.name+'</option>');
			}
			$('#select-firm').empty().append(sel);
			$('#select-firm').selectmenu("refresh");		
		});
	};

	var loadCustomers = function(){
		var sql = 'customers where firm like "%'+$('#select-firm').val()+'%" '
		var fields = ['customers.*'];

		DB.find(sql,fields,function(err, results){
			var sel = [];
			for(i=0;i<=results.rows.length-1;i++){
				var fields = results.rows.item(i);
				sel.push('<option value="'+fields.code+'">'+fields.name+'</option>');
			}
			$('#select-customer').empty().append(sel);
			$('#select-customer').selectmenu("refresh");		
		});
	};

	var loadProductTypes = function(){
		var sql = 'producttypes where firm like "%'+$('#select-firm').val()+'%" '
		var fields = ['producttypes.*'];

		DB.find(sql,fields,function(err, results){
			//console.log("rows=" + results.rows.length);
			var sel = [];
			for(i=0;i<=results.rows.length-1;i++){
				var fields = results.rows.item(i);
				sel.push('<option value="'+fields.code+'">'+fields.name+'</option>');
			}
			$('#select-producttype').empty().append(sel);
			$('#select-producttype').selectmenu("refresh");		
		});
	};

	$('#select-producttype').on('change',function(){
		$('#select-producttype').val();
	});
	
	$('#select-firm').on('change',function(){
		allPOs = null;
		allFactories = null;
		allVendors = null;
		var ul = $('#poselector');
		ul.html("");
		var ul = $('#vendorselector');
		ul.html("");
		var ul = $('#factoryselector');
		ul.html("");
		loadProductTypes();
		loadCustomers();
	});

	$('#main').off('blur').on('blur', '#text-reinspect_count, #po-text-orderqty', function(e){
		var val = $(this).val();
		$(this).val(parseInt(val));
	});
	
	$('#po-select-producttype_id').off('change').on('change', function(){
		if($.trim($(this).val())!=''){
			$('label[for="po-select-producttype_id"] > span').html(localization.get('producttype')).css({color:''});
		}else{
			$('label[for="po-select-producttype_id"] > span').html(localization.get('producttype') + ' '+localization.get('isrequired')+'').css({color:'#f00'});
		}
		$(this).selectmenu('refresh');
	});
	$('#po-text-po_no').off('change').on('change', function(){
		if($.trim($(this).val())!=''){
			$('label[for="po-text-po_no"] > span').html(localization.get('producttype')).css({color:''});
		}else{
			$('label[for="po-text-po_no"] > span').html(localization.get('po_no') + ' '+localization.get('isrequired')+'').css({color:'#f00'});
		}
	});

	console.log("Load Email Groups");

	var user = Storage.get('user-login');
	var emailgroup = user.emailgroup.toString().split(';');
	$.each(emailgroup, function(k,v){
		$('#select-report_email_recipient').append('<option value="'+$.trim(v)+'">'+$.trim(v)+'</option>');
	});
	$('#select-report_email_recipient').selectmenu('refresh');
	$('#text-inspector').val(user.name + " (" + user.cname + ")");
	$('#text-supervisor_email').val(user.supervisor);

	console.log("#select-report_email_recipient");
	console.log($('#select-report_email_recipient').val());
	console.log("Load Firms");

	loadFirms();

	if (App.page.main.copy == true)  {
		
		App.page.main.initInspectionDetailForCopy();
		console.log("Set Date for copy report");
		console.log(getCurrentDate());

		$('#text-inspection_date').val(getCurrentDate());
		console.log($('#text-inspection_date').val());

	} else {
		if(typeof App.page.main.InspectionData.id != 'undefined'){
			console.log("Load Inspection - Continue Initialization");
			App.page.main.getInspectionDetail();
			console.log(" Date");
			console.log($('#text-inspection_date').val());
		} else {
			console.log("Set Date");
			console.log(getCurrentDate());

			$('#text-inspection_date').val(getCurrentDate());
			console.log($('#text-inspection_date').val());
		}
	}
	console.log("Tag Supervisor Emails");

	$('input[name=text-supervisor_email]').attr('id', 'text-supervisor_email');
	if($('.tagsinput').length>0){
		$('.tagsinput').remove();
	}
	$('#text-supervisor_email').tagsInput({
		width: 'auto',
		delimiter : ';',
		onAddTag: function(elem)
		{
			if(!validateEmail(elem)){
				alert('Invalid email detected.');
				$('#text-supervisor_email').removeTag(elem);
			}
		},
		'defaultText':'Add'
	});

	console.log("Init Main Done");

};

var allFactories = null;
var allPOs = null;
var allVendors = null;
var poItems = [];
var vendorItems = [];
var factoryItems = [];

App.page.main.initSelectVendor = function(){
	var li = $('.li-select-vendor');	
	var ul = $('#vendorselector');
	
	var onClick = function(item){
		$('#po-text-vendor').val(item.code);
		App.page.main.save(false,true);
//		document.location = 'home.html';
//		$.mobile.changePage('#main',{
//			transition: 'slide',
//			reverse: 'true'
//		});
	};

	var isRunning=false;

	var list = function(searchtext){

		if (isRunning) {
			console.log("vendor list deduplicated");
			return;
		}
		isRunning = true;	

		var       html = "";

        var sql = 'vendors v left join linkfirmvendors lfv on v.code = lfv.vendorcode';
        var where = ' where lfv.firmcode="'+$('#select-firm').val()+'"';
        var	where2 = '';
        if(typeof searchtext != 'undefined'){
			where = " where code like '%"+searchtext+"%'";
		}
        var fields=['v.*'];
    	//console.log(sql+where+where2);

        DB.find(sql+where,fields,function(e,r){
			if(r.rows){
				vendorItems=[];
				if(r.rows.length>0){
					allVendors = r;
					console.log(r.rows.length + "rows");
					var nRows = r.rows.length;
					if (nRows > 100) {
						nRows = 100;
						html += "<li>displaying first 100 rows</li>";
						ul.html( html );
					}
					for(i=0;i<nRows;i++){
						var table = li.clone();
						vendorItems.push(r.rows.item(i));
						$.each(vendorItems[i], function(k,v){
							table.find('.'+k).html(v);
						});
						table.attr('id','po_'+i);
						ul.append(table);
					}
				} else {
					html += "<li>"+localization.get('notfound')+"</li>";
					ul.html(html);
				}
			} else {
				html += "<li>Error</li>";
				ul.html(html);
			}
            setTimeout(function(){
	            ul.listview( "refresh" );
	            ul.trigger( "updatelayout");
			},100);

            setTimeout(function(){
            	console.log("reset isRunning");
            	isRunning = false;
			},500);
            
        });
	};
	
	$('#btn_new_po').off('click').on('click', function(e){

		$('#po-text-po_no').val(inputpo);
		App.page.main.POData.po_number = $(inputpo).val();
		App.page.main.POData.firm_id = $('#select-firm').val();
		App.page.main.InspectionData.colorsize = {};
		$('label[for="po-text-po_no"] > span').html(localization.get('producttype')).css({color:''});
		$.mobile.changePage('#main',{
			transition: 'slide',
			reverse: 'true'
		});
	});

	var inputpo = "";
	$("#txt-search-vendor").val("");
	if (allVendors == null)
	{
		ul.html("<li>...</li>");
        ul.listview( "refresh" );
        ul.trigger( "updatelayout");
		vendorItems=[];
		list();
	}

	$('#selectvendor').off('click').on('click','.li-select-vendor',function(){
		var id = $(this).attr('id').split('_')[1];
		//console.log("CLICK");
		//console.log(vendorItems[id]);
		onClick(vendorItems[id]);
	});
	
	$('#txt-search-vendor').off('keyup').on('keyup', function(e){
		ul.html("<li>...</li>");
        ul.listview( "refresh" );
        ul.trigger( "updatelayout");
		var vals = $('#txt-search-vendor').val();
		waitForFinalEvent(function(){
			list(vals);
		}, 2000, "vendorselector");
	});
};

App.page.main.initSelectFactory = function(){
	var li = $('.li-select-factory');	
	var ul = $('#factoryselector');
	
	var onClick = function(item){
		$('#po-text-factory').val(item.factory);
		$('#text-factory_contacts_emailaddress').val(item.email);
		$('#text-factory_contacts_fullname').val(item.contact);
		App.page.main.save(false,true);
//		document.location = 'home.html';
//		$.mobile.changePage('#main',{
//			transition: 'slide',
//			reverse: 'true'
//		});
	};
	
	var isRunning=false;

	var list = function(searchtext){

		var html = "";

        var sql = 'factories';
        var where = ' where team like "%'+$('#select-firm').val()+'%"';
        var where2 = '';
        if(typeof searchtext != 'undefined'){
		   where2 = " and factory like '%"+searchtext+"%'";
		}
        var fields=['factories.*'];
	    //console.log(sql+where+where2);

		ul.html("");
        ul.listview( "refresh" );
        ul.trigger( "updatelayout");

        DB.find(sql+where+where2,fields,function(e,r){    

			if(r.rows){
				allFactories = r;
				factoryItems=[];
				if(r.rows.length>0){
					 console.log(r.rows.length + "rows");
					 var nRows = r.rows.length;
					 if (nRows > 100) {
					  nRows = 100;
					  html += "<li>displaying first 100 rows</li>";
					  //ul.html( html );
					 }
					 for(i=0;i<nRows;i++){
						  var table = li.clone();
						  factoryItems.push(r.rows.item(i));
					      $.each(factoryItems[i], function(k,v){
						      table.find('.'+k).html(v);
					      });
					      table.attr('id','po_'+i);
					      //ul.append(table);
						  html += '<li onclick="factoryOnClick('+i+')" class="li_select_factory" id="po_'+i+'">' + table.html() + "</li>";
				     }
				} else {
				     html += "<li>"+localization.get('notfound')+"</li>";
				     //ul.html(html);
			    }
		   } else {
				html += "<li>Error</li>";
			    //ul.html(html);
		   }
		   ul.html(html);

           setTimeout(function(){
             //console.log(ul.html());
             //console.log(html);
             ul.listview( "refresh" );
             ul.trigger( "updatelayout");
		   },100);

           setTimeout(function(){
             console.log("reset isRunning");
             isRunning = false;
		   },500);
        });

	};
	
	$('#btn_new_po').off('click').on('click', function(e){

		$('#po-text-po_no').val(inputpo);
		App.page.main.POData.po_number = $(inputpo).val();
		App.page.main.POData.firm_id = $('#select-firm').val();
		App.page.main.InspectionData.colorsize = {};
		$('label[for="po-text-po_no"] > span').html(localization.get('producttype')).css({color:''});
		$.mobile.changePage('#main',{
			transition: 'slide',
			reverse: 'true'
		});
	});

	$("#txt-search-factory").val("");
	
	if (allFactories == null) {
		ul.html("<li>...</li>");
        ul.listview( "refresh" );
        ul.trigger( "updatelayout");
		factoryItems = [];
		var inputpo = "";
		list();
	}
	$('#selectfactory').off('click').on('click','.li-select-factory',function(){
		var id = $(this).attr('id').split('_')[1];
		onClick(factoryItems[id]);
	});
	
	$('#txt-search-factory').off('keyup').on('keyup', function(e){
		ul.html("<li>...</li>");
        ul.listview( "refresh" );
        ul.trigger( "updatelayout");
		var vals = $('#txt-search-factory').val();
		waitForFinalEvent(function(){
			list(vals);
		}, 2000, "factoryselector");
	});
};

App.page.main.initSelectPO = function(){

	var li = $('.li-select-po');	
	var ul = $('#poselector');
	var totalQty=0;
	var po_list = [];
	var po_index = [];

	var selectState = 0;


	var onClick = function(item){
		
		Array.prototype.unique = function() {
		    var unique = [];
		    for (var i = 0; i < this.length; i++) {
		        if (unique.indexOf(this[i]) == -1) {
		            unique.push(this[i]);
		        }
		    }
		    return unique;
		};
		
		var fields = clone(item);
		var exclude = {
			'colorsize':''		
		};

		var po_no = fields["po_no"];	
		var style = fields["style"];	
		var qty = fields["qty"];
		var shipdate = fields["shipdate"];
		
		var indexstring = po_no+"|"+style+"|"+qty+"|"+shipdate;

		var selectedIndex = po_index.indexOf(indexstring)
		if(selectedIndex >= 0) // if newDog isn't already in the array
		{
			po_list.splice(selectedIndex, 1);
			po_index.splice(selectedIndex, 1);
			$('#po-text-po_no').val(po_list.unique().join("; "));
			$('#txt_selected_po').html(po_list.unique().join("; "));
			totalQty = 1*(totalQty) - 1*(qty);
			$('#txt_selected_po_qty').html(totalQty);
			$('#po-text-qty').val(totalQty);
			if ($('#po-text-po_no').val() == "") {
				$.each(fields, function(k,v){
					if (k != "po_no" && k != "qty") {
						//console.log("deleting " + k);
						$('#po-select-'+k).val("");
						$('#po-text-'+k).val("");
					}
				});
				try{
					$('select').selectmenu('refresh');
				}catch(e){
				}
			}
			//console.log("DELETE "+indexstring+" totalQty=" + totalQty);
			selectState = 2;

		} else {
			
			
			po_list.push(po_no);
			po_index.push(indexstring);

			
			$('#po-text-po_no').val(po_list.unique().join("; "));
			$('#txt_selected_po').html(po_list.unique().join("; "));

			if (totalQty == 0) {
				$.each(fields, function(k,v){
					if (k != "po_no" && k != "qty") {
						//console.log("populating " + k + ":" + v);
						var s = String(v);
						s = s.replace(/\._x000D_/g, "");
						s = s.replace(/_x000D_/g, "");
						s = s.replace(/^null$/gi, "");
						$('#po-select-'+k).val(s);
						$('#po-text-'+k).val(s);
					}
				});
				try{
					$('select').selectmenu('refresh');
				}catch(e){
				}
				
				$('label[for="po-text-po_no"] > span').html(localization.get('po_no')).css({color:''});
			}
			totalQty = 1*(totalQty) + 1*(qty);
			$('#txt_selected_po_qty').html(totalQty);
			$('#po-text-qty').val(totalQty);
			//console.log("ADD "+indexstring+" totalQty=" + totalQty);
			
			selectState = 1;
		}

	};
	
	var isRunning=false;

	var list = function(po){

		console.log("po list");

		if (isRunning) {
			console.log("po list deduplicated");
			return;
		}
		isRunning = true;	

		var       html = "";
		
        var sql = 'po';
        var where = ' where firm="'+$('#select-firm').val()+'" and customer = "'+$('#select-customer').val()+'"';
        var	where2 = '';
        if(typeof po != 'undefined'){
			where2 = " and (po_no like '%"+po+"%' or style like '%"+po+"%')";
		}
        var fields=['po.*'];
    	console.log(sql+where+where2);

        DB.find(sql+where+where2,fields,function(e,r){
			if(r.rows){

				console.log('app.js :  po list length - ' + r.rows.length);
				allPOs = r;
				poItems=[];
				if(r.rows.length>0){
					
					$('#btn_new_po').hide();
					var nRows = r.rows.length;
					if (nRows > 100) {
						nRows = 100;
						html += "<li>displaying first 100 rows</li>";
						ul.html(html);

						//console.log(html);

					}
					ul.empty();
					ul.html("");

					setTimeout(function(){
						for(i=0;i<nRows;i++){
							var table = li.clone();
							poItems.push(r.rows.item(i));							
							$.each(poItems[i], function(k,v){
								var s = String(v);
								s = s.replace(/\._x000D_/g, "");
								s = s.replace(/_x000D_/g, "");
								table.find('.'+k).html(s);								
							});
							table.attr('id','po_'+i);
							//html += table.html();
							//ul.append("<li>"+'po_'+i+"</li>");
							
							ul.append(table);
							
					        //ul.listview( "refresh" );
				            //ul.trigger( "updatelayout");
						}
						html = ul.html();
						//console.log(ul.html());
						//ul.html(html);
					},200);

		            setTimeout(function(){
			            ul.listview( "refresh" );
			            ul.trigger( "updatelayout");
					},1000);

		            setTimeout(function(){
		            	console.log("reset isRunning");
		            	isRunning = false;
					},1500);
				} else {
					$('#btn_new_po').show();
					html += "<li>"+localization.get('notfound')+"</li>";
					ul.html(html);
				}
			} else {
				html += "<li>Error</li>";
				ul.html(html);
			}
			

        });
	};

	$('#btn_select_po').off('click').on('click', function(e){
		allPOs = null;
		var ul = $('#poselector');
		ul.html("");

		$.mobile.changePage('#main',{
			transition: 'slide',
			reverse: 'true'
		});
	});

	$('#btn_new_po').off('click').on('click', function(e){
		$.each(App.page.main.POData, function(po_key, po_val){
			App.page.main.POData[po_key] = null;
			if($('#po-select-'+po_key).length!=0){
				$('#po-select-'+po_key).val('').selectmenu('refresh');
			}
			if($('#po-text-'+po_key).length!=0){
				$('#po-text-'+po_key).val('');
			}
		});
		$('#po-text-stylename').val('');
		$('#po-text-po_no').val(inputpo);
		App.page.main.POData.po_number = $(inputpo).val();
		App.page.main.POData.firm_id = $('#select-firm').val();
		App.page.main.InspectionData.colorsize = {};
		$('label[for="po-text-po_no"] > span').html(localization.get('producttype')).css({color:''});
		$.mobile.changePage('#main',{
			transition: 'slide',
			reverse: 'true'
		});
	});
	
	var inputpo = "";

	$("#txt-search-po").val("");

	if (allPOs == null) {
		poItems = [];
		ul.html("<li>...</li>");
        ul.listview( "refresh" );
        ul.trigger( "updatelayout");
		list();
	};
	
	$('#selectpo').off('click').on('click','.li-select-po',function(){
		var id = $(this).attr('id').split('_')[1];
		onClick(poItems[id]);
		if (selectState == 1)
			$(this).css("background", "#ddd");
		if (selectState == 2)
			$(this).css("background", "#fff");
	});
	
	$('#txt-search-po').off('keyup').on('keyup', function(e){
		var vals = $('#txt-search-po').val();
		ul.html("<li>...</li>");
        ul.listview( "refresh" );
        ul.trigger( "updatelayout");
		waitForFinalEvent(function(){
			list(vals);
		}, 1000, "poselector");
	});
};

App.page.main.initColorSize = function(){
	console.log("Init ColorSize");

	var updateCounter = function(){
		try{
			var colors = App.page.main.InspectionData.colorsize.colors;
			$('#div-colors-container').html('');
			$.each(colors, function(k, v){
				var html = [];
				var id = v.id;
				html.push('<input name="radio-colors" id="c'+id+'" type="radio">');
				html.push('<label for="c'+id+'">'+v.name+'</label>');
				$('#div-colors-container').append(html);
				$('#colorsize').page('destroy').page();
			});
		}catch(e){
		}
		
		try{
			var sizes = App.page.main.InspectionData.colorsize.sizes;
			$('#div-sizes-container').html('');
			$.each(sizes, function(k, v){
				var html = [];
				var id = v.id;
				html.push('<input data-name="'+v.name+'" data-spec="'+v.spec+'" data-qty="'+v.qty+'" name="radio-sizes" id="s'+id+'" type="radio">');
				html.push('<label for="s'+id+'" >'+v.name+'</label>');
				$('#div-sizes-container').append(html);
				$('#colorsize').page('destroy').page();
			});
		}catch(e){
		}
		var color = $('#div-colors-container').find('input[type=radio]');
		var size = $('#div-sizes-container').find('input[type=radio]');
		$('.totalColor').html(color.length);		
		$('.totalSize').html(size.length);	
	};
	
	var updateColor = function(){		
		//console.log("update color");
		$('#text-color-name').val('').focus();
		$('#colorsize').page('destroy').page();
		App.page.main.InspectionData.colorsize.colors = [];
		var i=0;
		$('#div-colors-container').find('input[type=radio]').each(function(){
			//console.log("id:"+$(this).attr('id'));
			//console.log("color:"+$(this).prev().html());
			i++;
			var colorid = "c"+i;
			App.page.main.InspectionData.colorsize.colors.push(
			{
				id: colorid,
				name: $(this).prev().html()
			}
			);
		});
	};
	var updateSize = function(){		
		$('#text-size-name').val('').focus();
		$('#colorsize').page('destroy').page();
		
		App.page.main.InspectionData.colorsize.sizes = [];
		var i=0;
		$('#div-sizes-container').find('input[type=radio]').each(function(){
			i++;
			var sizeid = "s"+i;
			App.page.main.InspectionData.colorsize.sizes.push(
			{
				id: sizeid,
				name: $(this).data('name')
			}
			);
		});
	};
	
	updateCounter();
	
	$('#btn-add-color').off('click').on('click', function(e){
		if($(this).html()==localization.get('update_color')){
			var selected = $('#div-colors-container').find('input[type=radio]:checked');
			selected.closest('.ui-radio').find('label').html($('#text-color-name').val());
			$('#btn-add-color').html('Add Color');
			$('#btn-delete-color').closest('.ui-block-b').remove();
		}else{
			if($.trim($('#text-color-name').val())==''){
				$('#text-color-name').css('border','1px solid #f00');
				return;
			}
			$('#text-color-name').css('border','');
			var html = [];
			var id = $('#div-colors-container').find('input[type=radio]').length+1;
			html.push('<input name="radio-colors" id="c'+id+'" type="radio">');
			html.push('<label for="c'+id+'">'+$('#text-color-name').val()+'</label>');
			$('#div-colors-container').append(html.join(''));
		}
		updateColor();
		updateCounter();
		App.page.main.save();
	});
	
	$('#btn-add-size').off('click').on('click', function(e){
		if($(this).html()==localization.get('update_size')){
			var selected = $('#div-sizes-container').find('input[type=radio]:checked');
			selected.closest('.ui-radio').find('label').html($('#text-size-name').val());
			$('#btn-add-size').html('Add Size');
			$('#btn-delete-size').closest('.ui-block-b').remove();
		}else{
			if($.trim($('#text-size-name').val())==''){
				$('#text-size-name').css('border','1px solid #f00');
				return;
			}
			// $('#text-size-qty').css('border','');
			// if($.trim($('#text-size-qty').val())==''){
				// $('#text-size-qty').css('border','1px solid #f00');
				// return;
			// }
			$('#text-size-name').css('border','');
			var html = [];
			var id = $('#div-sizes-container').find('input[type=radio]').length+1;
			html.push('<input data-name="'+$('#text-size-name').val()+'" name="radio-sizes" id="s'+id+'" type="radio">');
			html.push('<label for="s'+id+'" >'+$('#text-size-name').val()+'</label>');
			$('#div-sizes-container').append(html.join(''));
		}
		updateSize();
		updateCounter();
		App.page.main.save();
	});
	$('#div-sizes-container').off('change').on('change', '.ui-radio', function(e){
		//$('#text-size-name').val($(this).closest('.ui-radio').find('label').html()).focus();
		//$('#btn-add-size').html(localization.get('update_size'));

		$('#btn-delete-size').closest('.ui-block-b').remove();

		var parent = $('#btn-add-size').closest('.ui-grid-a');
		parent.append('<div id="size-b" class="ui-block-b"></div>');		
		$('<div class="ui-bar"><a id="btn-delete-size" href="javascript:void(0);" class="ui-btn ui-btn-inline ui-icon-plus ui-btn-icon-left"><span class="str_delete_color">'+localization.get('delete_size')+'</span></a></div>').appendTo('#size-b');

		$('#colorsize').page('destroy').page();
	});
	$('#text-size-name').off('click').on('click', function(e){
			// $('#btn-add-size').html(localization.get('add_size'));
			// $('#btn-delete-size').closest('.ui-block-b').remove();
			// $('#colorsize').page('destroy').page();
		});

	$('#div-colors-container').off('change').on('change', '.ui-radio', function(e){
		//$('#btn-add-color').html(localization.get('update_color'));

		$('#btn-delete-color').closest('.ui-block-b').remove();

		var parent = $('#btn-add-color').closest('.ui-grid-a');
		parent.append('<div id="color-b" class="ui-block-b"></div>');		
		$('<div class="ui-bar"><a id="btn-delete-color" href="javascript:void(0);" class="ui-btn ui-btn-inline ui-icon-plus ui-btn-icon-left"><span class="str_delete_color">'+localization.get('delete_color')+'</span></a></div>').appendTo('#color-b');
		//$('#text-color-name').val($(this).closest('.ui-radio').find('label').html()).focus();

		$('#colorsize').page('destroy').page();
	});
	$('#text-color-name').off('click').on('click', function(e){
			// $('#btn-add-color').html(localization.get('add_color'));
			// $('#btn-delete-color').closest('.ui-block-b').remove();
			// $('#colorsize').page('destroy').page();
		});
	

	
	$('#div_colors_import').off('click').on('click', '#btn-import-colorsize', function(e){
		try{
			if(navigator.connection.type != Connection.NONE && !_IS_RIPPLE_EMULATOR) {
				App.isOnline = true;
				$('.connectivity').removeClass('ui-icon-alert').addClass('ui-icon-check').html('<span class="str_nav_online">'+localization.strings.nav_online[Storage.get('locale')]+'</span>');
			}else{
				App.isOnline = false;
			}
		}catch(e){
			App.isOnline = false;
		}
		if (App.isOnline) {
			$('#popupImportColorsize').popup('open');
			$('#btn-import-colorsize-do').off('click').on('click', function(e){
				if ($("#import_inspection_no").val().length == 0) {
					alert(localization.get("noinspectionno"));
				}

				if (App.isOnline) {
					$.get("http://203.198.58.104/colorsize/api/api.php/colorsize/" + $("#import_inspection_no").val(), {}, function( res,status ) {
						if (status === "success") {
							
							if (res.length <= 2) {
								alert(localization.get("cannotfindinspectionno"));
							} else {
								//console.log(res.length);
								var cs = JSON.parse(res);
								$.each(cs, function(k,v){
									var csitem = v['color_size'];
									var n = csitem.indexOf(" / ");
									if (n > 0) {
										var color = csitem.substring(0,n);
										var size = csitem.substring(n+3);
										console.log("C:"+color+",S:"+size+";");
										var foundcolor=false;
										if (App.page.main.InspectionData.colorsize.colors) {
											$.each(App.page.main.InspectionData.colorsize.colors, function(k,v) {
												//console.log("C:"+v.name);
												if (v.name === color) {
													console.log("FOUND");
													foundcolor=true;
												}
											});
										} else {
											App.page.main.InspectionData.colorsize.colors = [];
										}

										var foundsize=false;
										if (App.page.main.InspectionData.colorsize.sizes) {
											$.each(App.page.main.InspectionData.colorsize.sizes, function(k,v) {
												//console.log("S:"+v.name);
												if (v.name === size) {
													console.log("FOUND");
													foundsize=true;
												}
											});
										} else {
											App.page.main.InspectionData.colorsize.sizes = [];
										}

										if (!foundcolor) {
											var html = [];
											var id = $('#div-colors-container').find('input[type=radio]').length+1;
											html.push('<input name="radio-colors" id="c'+id+'" type="radio">');
											html.push('<label for="c'+id+'">'+color+'</label>');
											$('#div-colors-container').append(html.join(''));
											var colorid = "c"+id;
											App.page.main.InspectionData.colorsize.colors.push(
											{
												id: colorid,
												name: color
											});
										}

										if (!foundsize) {
											var html = [];
											var id = $('#div-sizes-container').find('input[type=radio]').length+1;
											html.push('<input data-name="'+size+'" name="radio-sizes" id="s'+id+'" type="radio">');
											html.push('<label for="s'+id+'" >'+size+'</label>');
											$('#div-sizes-container').append(html.join(''));
											var sizeid = "s"+id;
											App.page.main.InspectionData.colorsize.sizes.push(
											{
												id: sizeid,
												name: size
											});
										}

									}
								});
								updateColor();
								updateSize();
								updateCounter();
								App.page.main.save();
							}
							
						} else {
							alert(localization.get("cannotimportcolorsize"));
						}	
					});
				} else {
					alert(localization.get("retryonline"));
				}
			});
		} else {
			alert(localization.get("retryonline"));
		}
	});

	$('#div_sizes').off('click').on('click', '#btn-delete-size', function(e){
		var selected = $('#div-sizes-container').find('input[type=radio]:checked');
		var selectedsize = selected.prev().html();
		selected.closest('.ui-radio').remove();
		$('#btn-add-size').html(localization.get('add_size'));
		$(this).closest('.ui-block-b').remove();

		$.each(App.page.main.InspectionData.colorsize_defect_points, function(dfsize, fields){
			var n = fields.colorsize.indexOf(" / ");
			if (n > 0) {
				var color = fields.colorsize.substring(0,n);
				var size = fields.colorsize.substring(n+3);
				if (size === selectedsize) {
					console.log("      DELETE  : " + color + " : " + size);
					delete (App.page.main.InspectionData.colorsize_defect_points[dfsize]);
				}
			}
		});	
		updateSize();
		updateCounter();
		App.page.main.save();
	});
	
	
	$('#div_colors').off('click').on('click', '#btn-delete-color', function(e){
		var selected = $('#div-colors-container').find('input[type=radio]:checked');
		var selectedcolor = selected.prev().html();
		console.log("SELECTED FOR DELETE : "+selectedcolor);
		selected.closest('.ui-radio').remove();
		$('#btn-add-color').html(localization.get('add_color'));
		$(this).closest('.ui-block-b').remove();
		
		$.each(App.page.main.InspectionData.colorsize_defect_points, function(dfsize, fields){
			var n = fields.colorsize.indexOf(" / ");
			if (n > 0) {
				var color = fields.colorsize.substring(0,n);
				var size = fields.colorsize.substring(n+3);
				if (color === selectedcolor) {
					console.log("      DELETE  : " + color + " : " + size);
					delete (App.page.main.InspectionData.colorsize_defect_points[dfsize]);
				}
			}
		});		
		
		updateColor();
		updateCounter();
		App.page.main.save();
	});	
	
};

App.page.main.initDefectPoints = function(){
	console.log("initDefectPoints");
	//console.log(App.page.main.POData.colorsize);
	//console.log(App.page.main.InspectionData.colorsize);
	var colors = App.page.main.InspectionData.colorsize.colors;
	var sizes = App.page.main.InspectionData.colorsize.sizes;

	var prev = $('#defect-color-size').val();
	
	var makeColorSizeIndex = function(str){
		var str;
		str = str.replace('/', '_')
		str = str.replace(/[^A-Za-z0-9_]*/g, '')
		return str;
	}
	
	$('#defect-color-size').html('<option class="default" selected="selected" value="">'+localization.get('select_color_n_size')+'</option>');
	var html = [];
	$.each(colors, function(k, v){
		$.each(sizes, function(k1, v1){
			html.push('<option value="'+makeColorSizeIndex(v.name+'_'+v1.name)+'">'+v.name+' / '+v1.name+'</option>');
		});
	});		
	
	$('#defect-color-size').append(html).val(prev).selectmenu('refresh');		
	var size = $('#defect-color-size').val();
		
	var defects = App.page.main.InspectionData.colorsize_defect_points;
	
	$('#defect-color-size').off('change').on('change', function(e){

		$('#defect_points').html("");
		
		$('#defect-color-size > .default').remove();
		size = $(this).val();	
		var cstext = $('#defect-color-size option:selected').text();

		var defect = null;
		try{
			$.each(defects, function(k,v){
				if (v.colorsize == cstext) {
					defect = v;
				}
			});
		}catch(e){
			defects = {};
			defects[size] = {};
			defect = defects[size];
		}
		if (defect == null) {
			defects[size] = {};
			defect = defects[size];
		}
		
		var defects_html = [];
		defects_html.push('<div class="ui-grid-a ui-corner-all defects" id="defects-'+size+'">');
		defects_html.push('	<div class="ui-block-a">');
		defects_html.push('		<div class="ui-bar">');
		defects_html.push('		<label for="text-basic">'+localization.get('lbl_colorsize')+'</label>');
		defects_html.push('		<input class="defect-val" type="text" name="defect-colorsize" id="'+size+'defect-colorsize" value="'+$(this).find('option:selected').text()+'">');
		defects_html.push('		<label for="text-basic">'+localization.get('quantity')+'</label>');
		defects_html.push('		<input class="defect-val" type="number" name="defect-quantity" id="'+size+'defect-quantity" value="">');
		defects_html.push('	</div>');
		defects_html.push('	</div>');
		defects_html.push('	<div class="ui-block-b">');
		defects_html.push('	<div class="ui-bar">');
		defects_html.push('		<label for="text-basic">'+localization.get('completedqty')+'</label>');
		defects_html.push('		<input class="defect-val" type="number" name="defect-completed_quantity" id="'+size+'defect-completed_quantity" value="">');
		defects_html.push('		<label for="text-basic">'+localization.get('inspectedqty')+'</label>');
		defects_html.push('		<input class="defect-val" type="number" name="defect-inspected_quantity" id="'+size+'defect-inspected_quantity" value="">');
		defects_html.push('	</div>');
		defects_html.push('	</div>');
		defects_html.push('</div><!-- /grid-a -->');
		// defects_html.push('<div class="ui-grid-b photos"></div>');
		defects_main = $('#defects_main').html(defects_html.join(''));
		
		if(!check(defect)){
			$.each(defect, function(k,v){
				//console.log("DEFECT " + k + ":" + v);
				if(k!='defect_items'){
					defects_main.find('#'+size+'defect-'+k).val(v);
				}
			});
		}
		
		DB.find('defectcategories',[],function(err, results){
			App.page.main.defectcategories = [];
			for(i=0;i<=results.rows.length-1;i++){
				var fields = results.rows.item(i);
				App.page.main.defectcategories[fields.id] = fields.id+' - '+fields.name + ' (' + fields.cname + ')';
			}
		});
		
		var oldcatid = "";
		
//		var sql = 'defectpoints where producttype like ' +
//			'"%;'+ $('#select-producttype').val()+';%"'+
//			' or  producttype like '+
//			'"%; '+$('#select-producttype').val()+ ';%"''
//			' or  producttype like '+
//			'"'+$('#select-producttype').val()+ ';%"'+
//			' or  producttype like '+
//			'"%; '+$('#select-producttype').val()+'"'
//			' order by seq';
		var pt = $('#select-producttype').val();
		var sql = 'defectpoints dp '
			+ 'WHERE dp.producttype like "%;'+pt+';%" '
			+ 'ORDER BY seqno';

		//console.log(sql);
		var fields=['dp.seq + 1 seqno, dp.*'];
		DB.find(sql,fields,function(err, results){
			for(i=0;i<=results.rows.length-1;i++){
				var fields = results.rows.item(i);
				try{
					saved = defect[fields.catid].defectpoints;
				}catch(e){
					saved = null;
				}
				var saveddefect = null;
				if(!check(saved)){
					$.each(saved, function(key, val){
						if (val.subcatid == fields.subcatid) {
							saveddefect = val;
						}
					});
				}
				if (saveddefect != null) {
					if (saveddefect.seqno == undefined || saveddefect.seqno === "undefined") {
						saveddefect.seqno = fields.seqno;
					}
					var f = {
						subcatid : saveddefect.subcatid,
						seqno : saveddefect.seqno,
						minor_qty : saveddefect.minor_qty,
						major_qty : saveddefect.major_qty,
						minor_remarks : saveddefect.minor_remarks,
						major_remarks : saveddefect.major_remarks,
						description : saveddefect.description,
						photos : saveddefect.photos
					};
				} else {
					var f = {
						subcatid : fields.subcatid,
						seqno : fields.seqno,
						minor_qty : '',
						major_qty : '',
						minor_remarks : '',
						major_remarks : '',
						description : fields.name + "<br>" + fields.cname,
						photos : []
					};	
				}
				create_defects(size,fields.catid,fields.id,f);
			}
			$("input").on('input', function() { 
				App.page.main.save();
			});
		});
			

		refreshPage();
		$(this).selectmenu('refresh');
		$('#div_defects').show();	
		$('.defect_points').show();
	});

	var initDefectPointsPhotos = function(c){
		if(check(App.page.main.InspectionData.colorsize_defect_points)){
			App.page.main.InspectionData.colorsize_defect_points = {};
		}
		if(check(App.page.main.InspectionData.colorsize_defect_points[size])){
			App.page.main.InspectionData.colorsize_defect_points[size] = {};
		}
		if(check(App.page.main.InspectionData.colorsize_defect_points[size][c])){
			App.page.main.InspectionData.colorsize_defect_points[size][c] = {};
		}
		if(check(App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints)){
			App.page.main.InspectionData.colorsize_defect_points[size][c]['defectpoints'] = {};
		}
	};

	function refreshPage() {
		$( "#defectpoints" ).trigger( "create" );
	}

	var create_defects = function(s,c,i,f){
		//console.log("CreateDefects "+s+":"+c+":"+i+":"+f);
		var html = [];
		
//		if (App.page.main.oldcatid != c) {
//			App.page.main.oldcatid = c;
//			html.push('<div class="ui-grid-a ui-corner-all defects" id="defects-'+size+'">');
//			html.push('	<div class="ui-block-a">');
//			html.push('   <div class="ui-bar">');
//			html.push('		<label>'+App.page.main.defectcategories[c]+'</label>');
//			html.push('	  </div>');
//			html.push('	</div>');
//			html.push('</div>');
//		}
		
		html.push('<div class="ui-grid  defect_points defect-points-'+s+'-'+c+'" id="'+s+'__'+c+'__'+i+'">');
		html.push('	<div class="ui-block">');
		html.push('		<div class="ui-bar">');
		html.push('			<div class="checkbox-group">');
		html.push('			<div class="ui-grid-b">');
		html.push('				<div class="ui-block-a" style="width:25%">');
		html.push('					<div class="ui-bar ui-select-defect" onclick="selectDefect(this)">');
		if (f.subcatid == undefined) {f.subcatid == ""};
		html.push('						<span class="txt-subcatid">'+f.subcatid+'</span>&nbsp;<font size=-2><span class="txt-seqno" style="color:#ddd">'+f.seqno+'</span></font><label><strong class="lbl-description">'+f.description+'</strong></label>');
		html.push('					</div>');
		html.push('				</div>');
		html.push('				<div class="ui-block-b" style="width:15%">');
		html.push('					<div class="ui-bar">');
		html.push('						<br/><label>'+localization.get('lbl_major')+'</label><label>'+localization.get('lbl_minor')+'</label>');
		html.push('					</div>');
		html.push('				</div>');
		html.push('				<div class="ui-block-b" style="width:17%">');
		html.push('					<div class="ui-bar">');
		html.push('						<legend>'+localization.get('quantity')+'</legend><input type="number" class="txt-major_qty" value="'+f.major_qty+'" /><input type="number" class="txt-minor_qty" value="'+f.minor_qty+'" />');
		html.push('					</div>');
		html.push('				</div>');
		html.push('				<div class="ui-block-c" style="width:43%">');
		html.push('					<div class="ui-bar">');
		html.push('						<legend>'+localization.get('remarks_reason')+'</legend><input type="text" class="txt-major_remarks" value="'+f.major_remarks+'" /><input type="text" class="txt-minor_remarks" value="'+f.minor_remarks+'" />');
		html.push('					</div>');
		html.push('				</div>');
		html.push('			</div>');
		html.push('			<div class="ui-grid-b photos"></div>');
		html.push('			</div>');
		html.push('		</div>');
		html.push('	</div>');
		html.push('</div>');
		// html.push('<div class="ui-grid-b photos"></div>');
		$('#defect_points').append(html.join(''));

		try{
			if(f.photos.length>0){
				$.each(f.photos,function(k,v){
					add_photos($('#'+s+'__'+c+'__'+i),v);
				});
			}
		}catch(e){
		}
		//console.log("refresh");
		refreshPage();
	};
	

	$('.defectpoints-capture,.defectpoints-browse').off('click').on('click', function(e){
		e.preventDefault();
		var c = $('#defect-defect-category').val();
		//var obj = $('#defectpoints').find('.defect-select-row:checked');	
		var obj = $('#defectpoints').find('.row_selected');			
		if(obj.length==0){
			return;
		}

		var id = obj.closest('.defect_points').attr('id');
		var c = id.split('__')[1];
		var field_id = id.split('__')[2];
		
		var count_img = 0;
		try{
			count_img = App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id].photos.length;
		}catch(e){
			count_img = 0;
		}
		if(count_img < 3){
			if($(this).hasClass('defectpoints-browse')){
				getPhoto(Camera.PictureSourceType.PHOTOLIBRARY, function(imgSource){
					try{
						add_photos(obj.closest('.defect_points'),'data:image/png;base64,'+ imgSource);
						if(check(App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id])){
							App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id] = {};
						}
						if(check(App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id].photos)){
							App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id].photos = [];
						}
						App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id].photos.push('data:image/png;base64,' + imgSource);
						App.page.main.save();
					}catch(e){
						// alert(e);
					}
				});
			}else{
				captureImage(function(imgSource){
					try{
						console.log("append captureImage");

						add_photos(obj.closest('.defect_points'),'data:image/png;base64,' + imgSource);
						if(check(App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id])){
							App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id] = {};
						}
						if(check(App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id].photos)){
							App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id].photos = [];
						}
						App.page.main.InspectionData.colorsize_defect_points[size][c].defectpoints[field_id].photos.push( 'data:image/png;base64,' + imgSource);	
						App.page.main.save();					

						console.log('defect capture : ' + size + "," + c + "," + field_id);
					}catch(e){						

						// alert(e);
					}
				});
			}
		} else {
			alert('You can only upload up to 3 photos');
		}
	});

	$('#defectpoints').off('blur').on('blur', 'input[type=number]', function(e){
		var val = $(this).val();
		$(this).val(parseInt(val));
	});
	$('#defectpoints').off('change').on('change', 'input[type=number].defect-val', function(e){
		App.page.main.hasDefectChange = true;
	});

	$('#add-defect-btn').off('click').on('click', function(e){
		var html = [];
		var c = $('#defect-defect-category').val();
		var id = $('.defect-points-'+size+'-'+c).length + 1;
		
		if (App.page.main.oldcatid != c) {
			App.page.main.oldcatid = c;
			html.push('<div class="ui-grid-a ui-corner-all defects" id="defects-'+size+'">');
			html.push('	<div class="ui-block-a">');
			html.push('   <div class="ui-bar">');
			html.push('		<label>Custom （其他）</label>');
			html.push('	  </div>');
			html.push('	</div>');
			html.push('</div>');
		}
		
		html.push('<div class="ui-grid  defect_points defect-points-'+size+'-'+c+'" id="'+size+'__'+c+'__0'+id+'">');
		html.push('	<div class="ui-block">');
		html.push('		<div class="ui-bar">');
		html.push('			<div class="checkbox-group">');
		html.push('				<div class="ui-grid-b">');
		html.push('					<div class="ui-block-a" style="width:25%">');
		html.push('					 <div class="ui-bar" onclick = "selectDefect(this)">');
		html.push('						<label><input type="text" value="" class="txt-description" /></label>');
		html.push('					</div>');
		html.push('				</div>');
		html.push('				<div class="ui-block-b" style="width:15%">');
		html.push('					<div class="ui-bar">');
		html.push('						<br/><label>'+localization.get('lbl_minor')+'</label><label>'+localization.get('lbl_major')+'</label>');
		html.push('					</div>');
		html.push('				</div>');
		html.push('				<div class="ui-block-b" style="width:17%">');
		html.push('					<div class="ui-bar">');
		html.push('						<legend>'+localization.get('quantity')+'</legend><input type="number" class="txt-minor_qty" value="" /><input type="number" class="txt-major_qty" value="" />');
		html.push('					</div>');
		html.push('				</div>');
		html.push('				<div class="ui-block-c" style="width:43%">');
		html.push('					<div class="ui-bar">');
		html.push('						<legend>'+localization.get('remarks_reason')+'</legend><input type="text" class="txt-minor_remarks" value="" /><input type="text" class="txt-major_remarks" value="" />');
		html.push('					</div>');
		html.push('				</div>');
		html.push('			</div>');		
		html.push('			<div class="ui-grid-b photos"></div>');
		html.push('			</div>');
		html.push('		</div>');
		html.push('	</div>');
		html.push('</div>');

		$('#defect_points').append(html.join(''));
		$('#defectpoints').page('destroy').page();

		console.log("refresh");
		refreshPage();

	});

	var initDefectCategory = function(){
		var elem  = '#defect-defect-category';
		DB.find('defectcategories',[],function(err, results){
				var sel = [];
			//sel.push('<option value="">None</option>');
			for(i=0;i<=results.rows.length-1;i++){
				var fields = results.rows.item(i);
				var saved = null;
				
				try{
					saved = defects[size][fields.id].defectpoints;
				}catch(e){
					saved = null;
				}
				if(!check(saved)&&Object.keys(saved).length>0)
					sel.push('<option style="background:#AFFFA5;" value="'+fields.id+'">'+fields.name + ' (' + fields.cname + ')' +' - ok</option>');
				else
					sel.push('<option value="'+fields.id+'">'+fields.name + ' (' + fields.cname + ')' +'</option>');
			}
			var p =$(elem).val();
			$(elem).html('<option class="default" value="">'+localization.get('select_defect_cat')+'</option>').append(sel).val(p).selectmenu('refresh');
		});	
	};
};

App.page.main.initFactory = function(){
	console.log("initFactory");
	

	
	$('.factory-capture,.factory-browse').off('click').on('click', function(e){
		console.log("factory_browse, factory_capture");
		e.preventDefault();
		//var obj = $('#factorycondition').find('.factory-select-row:checked');
		var container = $('#factorycondition').find('.row_selected');		

		if(container.length==0){
			return;
		}
		//var container = obj.closest('.div-factory');

		var id = $(container).attr('id');
		initFactoryPhotos(id);
		if(App.page.main.InspectionData[id].photos.length<3){
			if($(this).hasClass('factory-browse')){
				getPhoto(Camera.PictureSourceType.PHOTOLIBRARY, function(imgSource){
					$.mobile.loading('hide');
					add_photos(container, 'data:image/png;base64,'+imgSource);
					try{
						App.page.main.InspectionData[id].status = container.find('select.factory-status').val();						
						App.page.main.InspectionData[id].remarks = container.find('textarea').val();
						App.page.main.InspectionData[id].photos.push('data:image/png;base64,' + imgSource);	
						App.page.main.save();
					}catch(e){
					}
				});
			}else{
				captureImage(function(imgSource){
					$.mobile.loading('hide');
					add_photos(container,'data:image/png;base64,' + imgSource);
					try{
						console.log("append captureImage");
						App.page.main.InspectionData[id].status = container.find('select.factory-status').val();						
						App.page.main.InspectionData[id].remarks = container.find('textarea').val();
						App.page.main.InspectionData[id].photos.push('data:image/png;base64,' + imgSource);	
						App.page.main.save();					
					}catch(e){
						console.log("error captureImage");
					}
				});
			}
		} else {
			alert('You can only upload up to 3 photos');
		}
	});

	$('.div-factory').each(function(){
		var id = $(this).attr('id');
		initFactoryPhotos(id);

		var container = $(this);
		var photo_container = container.find('.photos');

		container.find('.remarks').hide();
		var block = ['a','b','c'];


		photo_container.html('');
		//console.log(App.page.main.InspectionData[id]);
		if(!check(App.page.main.InspectionData[id])){
			var status = App.page.main.InspectionData[id].status == undefined ? '1' : App.page.main.InspectionData[id].status;
			container.find('select.factory-status').val(status).selectmenu('refresh');
			container.find('textarea').val(App.page.main.InspectionData[id].remarks);

			if(App.page.main.InspectionData[id].status==2){
				container.find('.remarks').show();
			}
			try{
				var photoNo=0;
				$.each(App.page.main.InspectionData[id].photos, function(k,v){	
					
					photo_container.append($('<div onclick="removePhoto('+"'"+id+"',"+photoNo+',this)" class="ui-block-'+block[k]+'"><div class="ui-bar"><img id = "'+id+'-'+photoNo+'" src="'+v+'"></div></div>'));
					photoNo++;
					//photo_container.append('<div class="ui-block-'+block[k]+'"><div class="ui-bar"><img id = "'+id+'-'+photoNo+'" src="data:image/png;base64,'+ v +'"> </div></div>');
				});
			}catch(e){

			}
		}

		container.find('select.factory-status').off('change').on('change', function(e){
			if($(this).val()==2){
				container.find('.remarks').show();
			} else {
				container.find('.remarks').hide();				
			}
			App.page.main.InspectionData[id].status = container.find('select.factory-status').val();						
			$(this).selectmenu('refresh');
			App.page.main.save();
		});
		container.find('textarea').off('change').on('change', function(e){
			App.page.main.InspectionData[id].remarks = $(this).val();						
		});
	});

	$('#factorycondition').off('blur').on('blur', 'input[type=number]', function(e){
		var val = $(this).val();
		$(this).val(parseInt(val));
	});
	$("input").on('input', function() { 
		App.page.main.save();
	});
};

App.page.main.initMeasurements2 = function(){

	try{
		var colors = App.page.main.InspectionData.colorsize.colors;
		var sizes = App.page.main.InspectionData.colorsize.sizes;
	
		$('#measurement-color-size').html('<option selected="selected" value="">Select Color/Size</option>');
		var html = [];
		$.each(colors, function(k, v){
			$.each(sizes, function(k1, v1){
				html.push('<option data-qty="'+v1.qty+'" value="'+v.id+v1.id+'">'+v.name+'/'+v1.name+'</option>');
			});
		});		
		
		$('#measurement-color-size').append(html).selectmenu('refresh');		
		
		var size = '';
		
		
		var clicker = null;
		var isEditing = false;
		
		var set_headers = function(p){
			p = parseInt(p);
			var start = p + ((p-1) + (p-1));
			$('#first').html(start);
			$('#second').html(start+1);
			$('#third').html(start+2);
		}
		
		$('#keyboard123').popup({transition:'slideup'});
		
		$('#div-paging').off('click').on('click', 'a', function(e){
			var page = $(this).html();
			
			$('.show-keyboard').hide();
			$('#size-'+size).find('.page-'+page).show();
			set_headers(page);
		});
		
		$('#div-measurement-container').off('click').on('click', '.show-keyboard,.TOL_FROM,.TOL_TO,.SPECS', function(e){
			clicker = this;
			isEditing = false;
			$('#keyboard123').popup('open');
		});
		
		$('#keyboard123').off('click').on('click', 'a', function(e){
			e.preventDefault();
			var key = $(this).html();
			if(!isEditing){
				$(clicker).html(key);
				isEditing = true;
			} else {
				var fraction = '';
				if($(this).closest('.qckeys').hasClass('fractions')){					
					fraction = key;
					key = '';
				}else{
					fraction = $(clicker).data('fraction') == undefined ? '' : $(clicker).data('fraction');
				}
				var tmp = $(clicker).html();
				tmp = tmp.replace(' '+$(clicker).data('fraction'), '');
				$(clicker).data('fraction', fraction);
				$(clicker).html(tmp+key+' '+fraction);
			}
		});
		
		$('#measurement-add-row').off('click').on('click', function(e){
			e.preventDefault();
			var fields_b = [], fields_c = [], fields_d = [];
			fields_b.push('<div class="ui-block-c first-block"><div class="ui-bar">');
			fields_c.push('<div class="ui-block-c second-block"><div class="ui-bar">');
			fields_d.push('<div class="ui-block-c third-block"><div class="ui-bar">');
			
			var column_counter = 1;
			// alert(measurement_qty);
			for(i=0;i<=$('#size-'+size).find('.measurement-items').first().find('a').length-1;i++){
				var current_item = i + 1;
				var current_group = Math.ceil(current_item/3);
				column_counter = column_counter > 3 ? 1 : column_counter;
				if(column_counter%3==0){					
					fields_d.push('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;"></a>');							
				}else if(column_counter%2==0){					
					fields_c.push('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;"></a>');							
				}else{
					fields_b.push('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;"></a>');							
				}
				column_counter ++;
			}
			
			
			fields_b.push('</div></div>');
			fields_c.push('</div></div>');
			fields_d.push('</div></div>');
			
			var html = [];
			html.push('<div class="ui-grid-d measurement-items">');
			html.push('<label class="meas-item"><input type="text" value=""></label>');
			html.push('<div class="ui-block-a"><div class="ui-bar">');
			html.push('<table>');
			html.push('<tbody style="text-align:center;">');
			html.push('<tr>');
			html.push('<td><a style="padding:8px 30%; margin-top:3px auto; height:1em;" class="TOL_FROM ui-shadow ui-btn ui-corner-all ui-btn-inline">&nbsp;&nbsp;&nbsp;&nbsp;</a></td>');
			html.push('<td style="text-align:center;"><p>TO</p></td>');
			html.push('<td><a style="padding:8px 30%; margin-top:3px auto; height:1em;" class="TOL_TO ui-shadow ui-btn ui-corner-all ui-btn-inline">&nbsp;&nbsp;&nbsp;&nbsp;</a></td>');
			html.push('</tr>');
			html.push('</tbody>');
			html.push('</table>');
			html.push('</div></div>');
			html.push('<div class="ui-block-b"><div class="ui-bar">');
			html.push('<a class="SPECS" style="padding:8px; height:1em;"></a>');
			html.push('</div></div>');


			html.push(fields_b.join(''));
			html.push(fields_c.join(''));
			html.push(fields_d.join(''));
			html.push('</div><!-- /grid-b -->');
			html.push('<hr/>');
			$('#size-'+size).append(html.join(''));
			$('#size-'+size).find('.show-keyboard').hide();
			$('#size-'+size).find('.page-1').show();
			set_headers(1);
			$('#measurement').page('destroy').page();
		});

		$('#measurement').off('click').on('click', '#measurement-add-column', function(e){
			e.preventDefault();
			$('#size-'+size).find('.measurement-items').each(function(){
				var current_item = $(this).find('a.show-keyboard').length + 1;
				var counter = getNext3(current_item);
				
				var current_group = Math.ceil(current_item/3);
				if(counter%3==0){					
					$(this).find('.third-block > .ui-bar').append('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;"></a>');							
				}else if(counter%2==0){					
					$(this).find('.second-block > .ui-bar').append('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;"></a>');							
				}else{
					$(this).find('.first-block > .ui-bar').append('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;"></a>');							
				}
				var pages = Math.ceil(current_item/3);					
				
				$('#div-paging > .ui-controlgroup-controls').html('');
				$('#div-paging > .ui-controlgroup-controls').append('<a href="javascript:void(0);" class="paging ui-btn ui-corner-all ui-icon-carat-l ui-btn-icon-notext">1</a>');
				for(i=1;i<=pages;i++){
					$('#div-paging > .ui-controlgroup-controls').append('<a href="javascript:void(0);" class="paging ui-btn ui-corner-all">'+i+'</a>');
				}
				$('#div-paging > .ui-controlgroup-controls').append('<a href="javascript:void(0);" class="paging ui-btn ui-corner-all ui-icon-carat-r ui-btn-icon-notext">'+pages+'</a>');
				$(this).find('.show-keyboard').hide();
				$(this).find('.page-'+current_group).show();
				set_headers(current_group);
			});
			$('#measurement').page('destroy').page();
		});

		$('#measurement-color-size').off('change').on('change', function(e){
			if($(this).val()==''){
				$('#div-measurement').html('').fadeOut();
				return true;
			}
			size = $('#measurement-color-size').val();
			$('.block-size').hide();

			var measurement_qty = $('#measurement-color-size > option:selected').data('qty');

			if($('#size-'+size).length==1){
				$('#size-'+size).show();
				$('show-keyboard').hide();
				$('#size-'+size).find('.page-1').show();
				set_headers(1);
			}else{				
				var poms = null;
				try{
					poms = $.parseJSON(unescape(App.page.main.InspectionData.colorsize_POMs));
				}catch(e){
					poms = null;
				}
				//console.log(poms);
				var new_size = true;
				try{
					new_size = !cool(poms[size]);
				}catch(e){
					new_size = true;
				}
				//console.log(new_size);
				if(new_size){
					$.mobile.loading('show');
					DB.find('point_of_measures where product_id = ' + $('#po-select-producttype_id').val() ,[], function(err, results){
						if(err){
							// console.log(err);
						}
						if(results.rows){
							// console.log(measurement_qty);
							var fields_b = [], fields_c = [], fields_d = [];
							fields_b.push('<div class="ui-block-c first-block"><div class="ui-bar">');
							fields_c.push('<div class="ui-block-c second-block"><div class="ui-bar">');
							fields_d.push('<div class="ui-block-c third-block"><div class="ui-bar">');
							
							var column_counter = 1;
							// alert(measurement_qty);
							for(i=0;i<=measurement_qty-1;i++){
								var current_item = i + 1;
								var current_group = Math.ceil(current_item/3);
								column_counter = column_counter > 3 ? 1 : column_counter;
								if(column_counter%3==0){					
									fields_d.push('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;"></a>');							
								}else if(column_counter%2==0){					
									fields_c.push('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;"></a>');							
								}else{
									fields_b.push('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;"></a>');							
								}
								column_counter ++;
							}
							
							
							fields_b.push('</div></div>');
							fields_c.push('</div></div>');
							fields_d.push('</div></div>');
							
							// console.log(results.rows);
							var size_html = ['<div class="block-size" id="size-'+size+'">'];
							
							for(i=0;i<=results.rows.length-1;i++){
								var html = [];
								var item = results.rows.item(i);
								// console.log(item);
								html.push('<div class="ui-grid-d measurement-items">');
								html.push('<label class="meas-item">'+item.measurement+'</label>');
								html.push('<div class="ui-block-a"><div class="ui-bar">');
								html.push('<table>');
								html.push('<tbody style="text-align:center;">');
								html.push('<tr>');
								html.push('<td><a style="padding:8px 30%; margin-top:3px auto; height:1em;" class="TOL_FROM ui-shadow ui-btn ui-corner-all ui-btn-inline">&nbsp;&nbsp;&nbsp;&nbsp;</a></td>');
								html.push('<td style="text-align:center;"><p>TO</p></td>');
								html.push('<td><a style="padding:8px 30%; margin-top:3px auto; height:1em;" class="TOL_TO ui-shadow ui-btn ui-corner-all ui-btn-inline">&nbsp;&nbsp;&nbsp;&nbsp;</a></td>');
								html.push('</tr>');
								html.push('</tbody>');
								html.push('</table>');
								html.push('</div></div>');
								html.push('<div class="ui-block-b"><div class="ui-bar">');
								html.push('<a class="SPECS" style="padding:8px; height:1em;"></a>');
								html.push('</div></div>');


								html.push(fields_b.join(''));
								html.push(fields_c.join(''));
								html.push(fields_d.join(''));
								html.push('</div><!-- /grid-b -->');
								html.push('<hr/>');
								size_html.push(html.join(''));
							}
							size_html.push('</div>');
							$('#div-measurement-container').append(size_html.join(''));
							$('.show-keyboard').hide();

							$('#size-'+size).find('.page-1').show();
							set_headers(1);
							
							$('#div-measurement').fadeIn();
						}
						$.mobile.loading('hide');					
						
						var pages = Math.ceil(measurement_qty/3);					
						
						$('#div-paging > .ui-controlgroup-controls').html('');
						$('#div-paging > .ui-controlgroup-controls').append('<a href="javascript:void(0);" class="paging ui-btn ui-corner-all ui-icon-carat-l ui-btn-icon-notext">1</a>');
						for(i=1;i<=pages;i++){
							$('#div-paging > .ui-controlgroup-controls').append('<a href="javascript:void(0);" class="paging ui-btn ui-corner-all">'+i+'</a>');
						}
						$('#div-paging > .ui-controlgroup-controls').append('<a href="javascript:void(0);" class="paging ui-btn ui-corner-all ui-icon-carat-r ui-btn-icon-notext">'+pages+'</a>');
						
						$('#measurement').page('destroy').page();
					});
				} else {
					var size_html = ['<div class="block-size" id="size-'+size+'">'];
					$.each(poms[size], function(key, val){

						// console.log(measurement_qty);
						var fields_b = [], fields_c = [], fields_d = [];
						fields_b.push('<div class="ui-block-c first-block"><div class="ui-bar">');
						fields_c.push('<div class="ui-block-c second-block"><div class="ui-bar">');
						fields_d.push('<div class="ui-block-c third-block"><div class="ui-bar">');
						
						var column_counter = 1;
						// alert(measurement_qty);
						$.each(val.items, function(i, value){
							var current_item = i + 1;
							var current_group = Math.ceil(current_item/3);
							column_counter = column_counter > 3 ? 1 : column_counter;
							if(column_counter%3==0){					
								fields_d.push('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;">'+value+'</a>');							
							}else if(column_counter%2==0){					
								fields_c.push('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;">'+value+'</a>');							
							}else{
								fields_b.push('<a id="item-'+i+'" class="show-keyboard page-'+current_group+'" style="display:none; padding:8px; height:1em;">'+value+'</a>');							
							}
							column_counter ++;
						})
						
						
						fields_b.push('</div></div>');
						fields_c.push('</div></div>');
						fields_d.push('</div></div>');
						
						var html = [];
						html.push('<div class="ui-grid-d measurement-items">');
						html.push('<label class="meas-item">'+val.description+'</label>');
						html.push('<div class="ui-block-a"><div class="ui-bar">');
						html.push('<table>');
						html.push('<tbody style="text-align:center;">');
						html.push('<tr>');
						html.push('<td><a style="padding:8px 30%; margin-top:3px auto; height:1em;" class="TOL_FROM ui-shadow ui-btn ui-corner-all ui-btn-inline">&nbsp;&nbsp;'+val.TOL_FROM+'&nbsp;&nbsp;</a></td>');
						html.push('<td style="text-align:center;"><p>TO</p></td>');
						html.push('<td><a style="padding:8px 30%; margin-top:3px auto; height:1em;" class="TOL_TO ui-shadow ui-btn ui-corner-all ui-btn-inline">&nbsp;&nbsp;'+val.TOL_TO+'&nbsp;&nbsp;</a></td>');
						html.push('</tr>');
						html.push('</tbody>');
						html.push('</table>');
						html.push('</div></div>');
						html.push('<div class="ui-block-b"><div class="ui-bar">');
						html.push('<a class="SPECS" style="padding:8px; height:1em;">'+val.SPECS+'</a>');
						html.push('</div></div>');


						html.push(fields_b.join(''));
						html.push(fields_c.join(''));
						html.push(fields_d.join(''));
						html.push('</div><!-- /grid-b -->');
						html.push('<hr/>');
						size_html.push(html.join(''));
						var pages = Math.ceil(val.items.length/3);					
						
						$('#div-paging > .ui-controlgroup-controls').html('');
						$('#div-paging > .ui-controlgroup-controls').append('<a href="javascript:void(0);" class="paging ui-btn ui-corner-all ui-icon-carat-l ui-btn-icon-notext">1</a>');
						for(i=1;i<=pages;i++){
							$('#div-paging > .ui-controlgroup-controls').append('<a href="javascript:void(0);" class="paging ui-btn ui-corner-all">'+i+'</a>');
						}
						$('#div-paging > .ui-controlgroup-controls').append('<a href="javascript:void(0);" class="paging ui-btn ui-corner-all ui-icon-carat-r ui-btn-icon-notext">'+pages+'</a>');
						
					});
					$('#div-measurement-container').append(size_html.join(''));

					$('.show-keyboard').hide();

					$('#size-'+size).find('.page-1').show();

					$('#div-measurement').fadeIn();

					$('#measurement').page('destroy').page();
				}
			}

		});

		$('#link-result').off('click').on('click', function(){
			try{
				var Factory_NoImages = [];
				$('#factorycondition').find('.div-factory').each(function(){
					var photo_container = $(this).find('.photos');
					if(photo_container.find('img').length==0){
						Factory_NoImages.push('<strong>'+$(this).find('strong').html()+'</strong>' + ' ' + localization.get('hasnophoto'));
					}
				});

				// if(Factory_NoImages.length>0){
					// $('#popup-items').html(Factory_NoImages.join('<br/>'));
					
					// $.mobile.changePage('#popupResult', {
						// transition: 'pop'
					// });
					// return;
				// }
				// var poms = null;
				// try{
					// poms = $.parseJSON(unescape(App.page.main.InspectionData.colorsize_POMs));
				// }catch(e){
					// poms = {};
				// }
				// if($('.block-size').length == 0 && poms.length==0){
					// $('#popup-items').html('You did not enter any measurements.');
					
					// $.mobile.changePage('#popupResult', {
						// transition: 'pop'
					// });
					// return;
				// }
				$.mobile.changePage("#result", {
					transition: "slide"
				});
			}catch(e){
				alert(e);
			}
		});
	}catch(e){
	}	
};

//
//	For Trim page
//
App.page.main.initTrim = function(){
	console.log("initTrim");

	var trimcontainer = $('.trim-container');
	var trimcssclass = ["ui-block-a", "ui-block-b", "ui-block-c"];

	trimcontainer.empty();

	if (App.page.main.InspectionData.trim == undefined) {
		App.page.main.InspectionData.trim ={};
	}

	DB.find('trims',['trims.*'], function(err,results){
		
		if (results != undefined) {
			var html = '<div class="ui-grid-b">';
			for (var i=0;i<results.rows.length;i++) {
				var item = results.rows.item(i);
				var cssindex = i % 3;
				var isChecked = "";
				var inputtype = "checkbox";

				var thistrim = App.page.main.InspectionData.trim["trim-"+item.subcatid];
				if (thistrim && thistrim.checked=="yes") {
					isChecked = 'checked="yes" data-cacheval="false"';
				}

				var pre="";
				var post="";
				var val = "";

				if (item.fieldtype == "I") {
					//console.log(item);
					inputtype = "text";
					pre = '<div class="ui-field-contain">';
					post = '</div>';
					if (App.page.main.InspectionData.trim["trim-"+item.subcatid]) {
						val = ' value="' + App.page.main.InspectionData.trim["trim-"+item.subcatid]['val'] + '"';
					}
					
				}

				html += '<div class="'+trimcssclass[cssindex]+'">'
					+ pre
					+ '<label class="trimlabel" data-desc-en="' + item.desc_en 
					+ '" data-desc-cn="'+item.desc_cn
					+ '" data-trimtype="'+item.fieldtype+'" for="trim-'+item.subcatid+'">'
					+ item.desc_en
					+ '<br>'
					+ item.desc_cn
					+ '</label>'
					+ '<input class = "triminput" type="'
					+ inputtype +'" '
					+ isChecked+' onclick="clickTrim(this)" '
					+ val
					+ ' data-iconpos="right" id="trim-'+item.subcatid+'"/>'
					+ post
					+ '</div>';
			}
			html += '</div>'
		}

		trimcontainer.append(html).trigger('create');

		$('.triminput').each(function(index) {
		   $(this).bind("input paste", function(event){
		   		clickTrim(this);
		   });
		});

	});

}

App.page.main.initMeasurements = function(){
	console.log("initMeasurements");
	var measurements_photos = $('#measurement').find('.measurement-photos');
	var container = null;
	container = measurements_photos;
	$('.photos-by-sizes').hide();
	container.show();
	container.html('');
	var numFactoryPhotos=0;
	
	try{
		var photos = App.page.main.InspectionData.factory_photos;
		if(photos.length>0){
			numFactoryPhotos = photos.length;
			var i=0;
			$.each(photos, function(k,v){
				container.append('<div class="ui-bar" id="factoryphoto-'+i+'" onclick="removeFactoryPhoto(this,'+i+')"><img src="'+ v +'"> </div>');
				i++;
			});
		}else{
			//container.append('<h3>'+localization.get('nophoto')+'</h3>');
		}
	}catch(e){
		//container.append('<h3>'+localization.get('nophoto')+'</h3>');
	}
		
	$('.measurement-capture,.measurement-browse').off('click').on('click', function(e){
		e.preventDefault();
		try{
			var storeToJSON = function(imgSource){
				console.log("POM storeToJSON");

				try{
					if(check(App.page.main.InspectionData.factory_photos)){
						App.page.main.InspectionData.factory_photos = [];
						//console.log("CHECK 1");
					}
					if(JSON.stringify(App.page.main.InspectionData.factory_photos) == '{}'){
						App.page.main.InspectionData.factory_photos = [];
						//console.log("CHECK 1");
					}
					//console.log(App.page.main.InspectionData.factory_photos);
					//console.log(JSON.stringify(App.page.main.InspectionData.factory_photos));
					try{ 
						App.page.main.InspectionData.factory_photos.push('data:image/png;base64,'+imgSource);						
						App.page.main.save();
					}catch(e){
						// alert(e);
					}
				}catch(e){
					// alert(e);
					// console.log(e);
				}
			};
			
			if($(this).hasClass('measurement-browse')){
				getPhoto(Camera.PictureSourceType.PHOTOLIBRARY, function(imgSource){
					$.mobile.loading('hide');
					try{
						container.append($('<div class="ui-bar" id="factoryphoto-'+numFactoryPhotos+'" onclick="removeFactoryPhoto(this,'+numFactoryPhotos+')"><img src="data:image/png;base64,'+ imgSource +'"> </div>'));
						storeToJSON(imgSource);
						App.page.main.save();
					}catch(img_error){
						// alert(img_error);
						// console.log(img_error);
					}
				});
			}else{
				captureImage(function(imgSource){
					$.mobile.loading('hide');
					try{
						container.append($('<div class="ui-bar" id="factoryphoto-'+numFactoryPhotos+'" onclick="removeFactoryPhoto(this,'+numFactoryPhotos+')"><img src="data:image/png;base64,'+ imgSource +'"> </div>'));
						storeToJSON(imgSource);
						App.page.main.save();
					}catch(img_error){
						// alert(img_error);
						// console.log("error captureImage");
					}
				});
			}
		}catch(e){
			// alert(e);
		}
	});

	$('#link-result').off('click').on('click', function(){
		try{
			var Factory_NoImages = [];
			var Factory_NoImages = [];
			$('#factorycondition').find('.div-factory').each(function(){
				try{
					var photo_container = App.page.main.InspectionData[$(this).attr('id')];
					if(photo_container.photos.length==0){
						Factory_NoImages.push('<strong>'+$(this).find('strong').html()+'</strong>' + ' ' + localization.get('hasnophoto'));
					}
				}catch(e){
				}
			});

			$('#popupResult').off('click').on('click','.continue', function(e){
				$.mobile.changePage("#result", {
					transition: 'slide'
				});
			});

			if(Factory_NoImages.length>0){
				$('#popup-items').html(Factory_NoImages.join('<br/>'));
				$.mobile.changePage('#popupResult', {
					transition: 'pop'
				});
				return;
			}
			$.mobile.changePage("#result", {
				transition: "slide"
			});
		}catch(e){
				//console.log(e);
		}
	});
};

App.page.main.initResult = function(){
	
	$('#link-result-save').off('click').on('click', function(e){
		e.preventDefault();
		App.page.main.save(true);
	});
	
	$('#link-confirm-no').off('click').on('click', function(e){
		App.page.main.save(false,true);

//		setTimeout(function(){
//			location.href = 'home.html';
//		},1000);
	});
	$('#link-confirm-yes').off('click').on('click', function(e){
		redirect('signing.html#'+App.page.main.InspectionData.id);
	});	
	$('input[name=text-factory_contacts_emailaddress]').attr('id', 'text-factory_contacts_emailaddress');
	if($('.tagsinput').length>0){
		$('.tagsinput').remove();
	}
	$('#text-factory_contacts_emailaddress').tagsInput({
		width: 'auto',
		delimiter : ';',
		onAddTag: function(elem)
		{
			if(!validateEmail(elem)){
				alert('Invalid email detected.');
				$('#text-factory_contacts_emailaddress').removeTag(elem);
			}
		},
		'defaultText':'Add'
	});
};

App.page.main.save = function(done,home,post){
	$("h1").css({'color':'#f88'});
	waitForFinalEvent(function(){
		App.page.main.do_save(done,home,post);
	}, 1000, "save");
};

App.page.main.do_save = function(done,home,post){
	console.log("App.page.main.save");

	App.page.main.InspectionData.po_no = $('#po-text-po_no').val();

	if (App.page.main.InspectionData.po_no == null) {
		return;
	}

	var initDefectPointsVar = function(){
		console.log ("Saving Defect Points");
		$('.defects').each(function(){
			var size = $(this).attr('id').split('-')[1];
			//console.log ("- size : " + size );			
			$(this).find('input').each(function(){
				var field = $(this).attr('id').split('-')[1];
				//console.log(field + " : " + $(this).val());
				if(typeof App.page.main.InspectionData.colorsize_defect_points != 'object'){
					App.page.main.InspectionData.colorsize_defect_points = {};
				}
				try{
					App.page.main.InspectionData.colorsize_defect_points[size][field] = $(this).val();
				} catch(e){
					try{
						App.page.main.InspectionData.colorsize_defect_points[size] = {};
						App.page.main.InspectionData.colorsize_defect_points[size][field] = $(this).val();
					}catch(e){							
						App.page.main.InspectionData.colorsize_defect_points = {};
						App.page.main.InspectionData.colorsize_defect_points[size] = {};
						App.page.main.InspectionData.colorsize_defect_points[size][field] = $(this).val();
					}
				}
			});
		});

		console.log("Defect Points");
		
		$('#text-totalcompletedqty').val(0)
		$('#text-totalinspectedqty').val(0);
		$('#text-totalrejectedqty-minor').val(0);
		$('#text-totalrejectedqty-major').val(0);
		// BERNARD
		// To do : colorsize_defect_points[colorsize][category] is a 2 dimensional array
		// we need to avoid duplication of color size here
		// we do that by finding the array for exisitng color size, not by blindly accessing the array
		// because colorsize here uses "color_1size_1", "color_1size_2" etc and things will shift
		// after we add a color or size
		
		$('.defect_points').each(function(){

			var defect_id = $(this).attr('id').split('__');
			console.log("===  DEFECT POINT : " + defect_id);
			var description  = $(this).find('.txt-description').length == 0 ? $(this).find('.lbl-description').html() : $(this).find('.txt-description').val();
			var minor_qty = $(this).find('.txt-minor_qty').val();
			var minor_remarks = $(this).find('.txt-minor_remarks').val();
			var major_qty = $(this).find('.txt-major_qty').val();
			var major_remarks = $(this).find('.txt-major_remarks').val();
			var subcatid = $(this).find('.txt-subcatid').text();
			var seqno = $(this).find('.txt-seqno').text();

			if (minor_qty > 0 || major_qty > 0 || minor_remarks.length > 0 || major_remarks.length > 0) {
				if(typeof App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]] != 'object'){
					App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]] = {};
				}
				if(typeof App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]].defectpoints != 'object' ){
					App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]['defectpoints'] = {};
				}
				if(typeof App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]].defectpoints[defect_id[2]] != 'object'){
					App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]].defectpoints[defect_id[2]] = {};
				}
				try{
					App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]['defectpoints'][defect_id[2]].seqno = seqno;
					App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]['defectpoints'][defect_id[2]].subcatid = subcatid;
					App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]['defectpoints'][defect_id[2]].minor_qty = minor_qty;
					App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]['defectpoints'][defect_id[2]].minor_remarks = minor_remarks;
					App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]['defectpoints'][defect_id[2]].major_qty = major_qty;
					App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]['defectpoints'][defect_id[2]].major_remarks = major_remarks;
					App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]['defectpoints'][defect_id[2]].description = description;
				}catch(e){						
				}
			} else {
				if (App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]) {
					if (App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]['defectpoints']) {
						if (App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]].defectpoints[defect_id[2]]) {
							delete (App.page.main.InspectionData.colorsize_defect_points[defect_id[0]][defect_id[1]]);
						}
					}
				}
			}
		});

		try{
			$.each(App.page.main.InspectionData.colorsize_defect_points, function(dfsize, fields){
				
				console.log("===== DFSIZE : " + dfsize + " : " + fields.colorsize);
				console.log(JSON.stringify(fields,null," "));

				fields.rejected_quantity_minor= 0;
				fields.rejected_quantity_major = 0;

				var nDPs=0;
				$.each(fields, function(k, v){
				 if(typeof v.defectpoints == 'object'){
				  $.each(v.defectpoints, function(k1, v1){
					  nDPs++;
					  fields.rejected_quantity_minor += number(v1.minor_qty);
					  fields.rejected_quantity_major += number(v1.major_qty);
				  });
				 }
				});
				
//				if (nDPs == 0) {
//					delete(App.page.main.InspectionData.colorsize_defect_points[dfsize]);
//				}
				
				
				var n = fields.colorsize.indexOf(" / ");
				if (n > 0) {
					var color = fields.colorsize.substring(0,n);
					var size = fields.colorsize.substring(n+3);
					
					var bFoundSize=false;
					var bFoundColor=false;
					try{
						var colors = App.page.main.InspectionData.colorsize.colors;
						$.each(colors, function(k, v){
							if (v.name === color) {
								bFoundColor=true;
							}
						});
					}catch(e){
					}
					
					try{
						var sizes = App.page.main.InspectionData.colorsize.sizes;
						$.each(sizes, function(k, v){
							if (v.name === size) {
								bFoundSize=true;
							}
						});
					}catch(e){
					}
					
					if (!bFoundColor || !bFoundSize) {
						console.log("      DELETE  : " + color + " : " + size);
						//delete (App.page.main.InspectionData.colorsize_defect_points[dfsize]);
					}
				}

				$('#text-totalcompletedqty').val(number($('#text-totalcompletedqty').val())+number(fields.completed_quantity));
				$('#text-totalinspectedqty').val(number($('#text-totalinspectedqty').val())+number(fields.inspected_quantity));
				$('#text-totalrejectedqty-minor').val(number($('#text-totalrejectedqty-minor').val())+number(fields.rejected_quantity_minor));
				$('#text-totalrejectedqty-major').val(number($('#text-totalrejectedqty-major').val())+number(fields.rejected_quantity_major));
			});
		}catch(e){
		}

		if(App.page.main.hasDefectChange==true){
//			$('#text-totalcompletedqty').val(completed_quantity);
//			$('#text-totalinspectedqty').val(inspected_quantity);
//			$('#text-totalrejectedqty-minor').val(rejected_quantity_minor);
//			$('#text-totalrejectedqty-major').val(rejected_quantity_major);			
		}		
	};		

	var getInspectionNumber = function(){
		if(App.page.main.InspectionData.inspection_serial_id=='null'||App.page.main.InspectionData.inspection_serial_id==null){
			var d = new Date();
			var y = d.getFullYear(); 
			var dd = d.getDate();
			var mm = d.getMonth()+1; 
			if(dd < 10){ dd = '0' + dd };
			if(mm < 10){ mm = '0' + mm }; 
			var timeStamp = d.getTime();
			var date = new Date(timeStamp);
			var hours = date.getHours();
			var minutes = date.getMinutes();
			var seconds = date.getSeconds();
			timeStamp = hours.toString() + minutes.toString() +  seconds.toString();
			return Storage.get('user-login').id + '-' + y.toString() + mm.toString() + dd.toString() + '-' + timeStamp.toString();
		} else {
			return App.page.main.InspectionData.inspection_serial_id;
		}
	};

	var initPOData = function(){
		var po = clone(App.page.main.POData);
		po.colorsize = escape(JSON.stringify(App.page.main.POData.colorsize));
		$.each(App.page.main.POData, function(k, v){
			if(k != '123455'&&k!='__proto__'){
				if($('#po-text-'+k).length!=0){
					po[k] = $('#po-text-'+k).val()
				}
				if($('#po-select-'+k).length!=0){
					po[k] = $('#po-select-'+k).val()
				}
			}else{
				console.log('weird');
			}
		});
		return po;
	};

	var initInspectionData = function(){
		var i = clone(App.page.main.InspectionData);
		$.each(App.page.main.InspectionData, function(k, v){
			if($('#po-text-'+k).length!=0){
				i[k] = $('#po-text-'+k).val();
			}
			if($('#po-select-'+k).length!=0){
				i[k] = $('#po-select-'+k).val();
			}
			if($('#text-'+k).length!=0){
				i[k] = $('#text-'+k).val();
			}
			if($('#select-'+k).length!=0){
				i[k] = $('#select-'+k).val();
			}
		});
		return i;
	};
	
	/*
	var initPOMS = function(){
		try{
			poms = App.page.main.InspectionData.colorsize_POMs;
		}catch(e){
			poms = {};
		}

		var replaceNbsps = function(str) {
			return str.replace(/&nbsp;/g, '');
		};

		$('.block-size').each(function(){
			var size = $(this).attr('id').split('-')[1];
			try{
				poms[size] = [];
			}catch(e){
				poms = {};
				poms[size] = [];
			}
			$(this).find('.measurement-items').each(function(){
				var description = $(this).find('input[type=text]').length == 0 ? $(this).find('label').html() : $(this).find('input[type=text]').val();
				var aitems = $(this).find('a.show-keyboard');				
				var pom = {};
				pom['description'] = description;
				pom['TOL_FROM'] = replaceNbsps($(this).find('.TOL_FROM').html());
				pom['TOL_TO'] = replaceNbsps($(this).find('.TOL_TO').html());
				pom['SPECS'] = $(this).find('.SPECS').html();
				pom['items'] = [];
				for(i=0;i<=aitems.length-1;i++){
					pom.items.push($(this).find('#item-'+i).html());
				}
				poms[size].push(pom);
			});
		});

		return poms;
	};

	var savePO = function(callback){
		if(App.page.main.POData.id!=null){
			console.log("Saving PO #"+App.page.main.POData.id);
			delete poData.po_no; // do not overwrite po no
			poData.syncstatus = 2;
			DB.update('po',{id:poData.id},poData,callback);
		}else {			
			console.log("Adding New PO");
			delete POData.id;
			poData.syncstatus = 1;
			var items = [];
			items.push(poData);
			DB.single_insert('po',items,callback);
		}
	};
	*/			
	var inspectionNum = getInspectionNumber();	
	var inspectionData = initInspectionData();
	var poData = initPOData();

	initDefectPointsVar();

	//savePO(function(e,r,insertId){

	var json_objects = {
		'general_cleanliness':'general_cleanliness',
		'workflow_cleanliness':'workflow_cleanliness',
		'workflow_ventilation':'workflow_ventilation',
		'workflow_lighting':'workflow_lighting',
		'child_labor':'child_labor',
		'fabric_relaxation':'fabric_relaxation',
		'broken_needle_record':'broken_needle_record',
		'colorsize':'colorsize',
		'colorsize_POMs':'colorsize_POMs',
		"colorsize_defect_points": "colorsize_defect_points",
		"factory_photos": "factory_photos",
		//"inspector_sign": "inspector_sign",
		//"factory_sign": "factory_sign"
	};
	$.each(json_objects, function(k,v){
		inspectionData[k] = escape(JSON.stringify(App.page.main.InspectionData[k]));
		//console.log(k+":"+unescape(inspectionData[k]));			
	});

	inspectionData['colorsize_POMs']= escape(JSON.stringify(App.page.main.InspectionData.colorsize_POMs));	

	inspectionData['trim']= escape(JSON.stringify(App.page.main.InspectionData.trim));	
	
	inspectionData.email = Storage.get('user-login').email;
	inspectionData.style = String($('#po-text-style').val());
	inspectionData.supervisor_email = Storage.get('user-login').supervisor;
	inspectionData.hksupervisor_email = Storage.get('user-login').hksupervisor;
	inspectionData.report_email_recipient = $('#select-report_email_recipient').val();
	inspectionData.factory_contacts_emailaddress = $('#text-factory_contacts_emailaddress').val();
	inspectionData.firm = $('#select-firm').val();
	inspectionData.styledesc = $('#po-text-styledesc').val();
	inspectionData.shipdate = $('#po-text-shipdate').val();
	inspectionData.shipmode = $('#po-text-shipmode').val();
	inspectionData.factory = $('#po-text-factory').val();
	inspectionData.destination = $('#po-text-destination').val();
	inspectionData.vendor = $('#po-text-vendor').val();
	//inspectionData.customer = $('#po-text-customer').val();
	inspectionData.customer = $('#select-customer').val();

	inspectionData.qty = $('#po-text-qty').val();
	inspectionData.producttype = $('#select-producttype').val();

	inspectionData.fibercontent = $('#po-text-fibercontent').val();
	inspectionData.garmentweight = $('#po-text-garmentweight').val();
	inspectionData.totalnumworkers = $('#text-totalnumworkers').val();

	inspectionData.intime = $('#text-intime').val();
	inspectionData.outtime = $('#text-outtime').val();
	inspectionData.factory_contacts_fullname = $('#text-factory_contacts_fullname').val();
	inspectionData.factory_contacts_title = $('#text-factory_contacts_title').val();
	inspectionData.factory_qc_name = $('#text-factory_qc_name').val();
	inspectionData.factory_contacts_emailaddress = $('#text-factory_contacts_emailaddress').val();
	inspectionData.inspection_status = $('#select-inspection_status').val();
	inspectionData.qc_status = $('#select-qc_status').val();
	inspectionData.qc_remarks = $('#text-qc_remarks').val();

	inspectionData.tileimage = $('#select_inspection_report_image').attr('src');		 
	
	inspectionData.date_created = App.page.main.InspectionData.date_created;
	inspectionData.date_modified = App.page.main.InspectionData.date_modified;
	console.log("TIMESTAMP");
	console.log(inspectionData.date_created);
	console.log(inspectionData.date_modified);
	getGPSLocation(function(Loc) {
	      
		inspectionData.GPSLongitude = Loc.coords.longitude;
		inspectionData.GPSLatitude = Loc.coords.latitude;		
	});

	if(cool(inspectionData.id)){
		
		console.log("Updating Inspection #"+inspectionData.id);
		//delete inspectionData.date_created;
		App.page.main.InspectionData.date_modified = getCurrentDateTime();
		inspectionData.date_modified = getCurrentDateTime();
		DB.update('inspections',{id:inspectionData.id},inspectionData,function(err,results){
			//console.log(err);
			//console.log(results);
			$("h1").css({'color':'#fff'});
			if(done!=undefined) redirect('view.html#'+App.page.main.InspectionData.id);
		});
	}else {			
		console.log("Creating New Inspection - inpsectionNum : " + inspectionNum );
		delete inspectionData.id;
		inspectionData.date_created = getCurrentDateTime();
		inspectionData.inspection_serial_id = inspectionNum;

		var items = [];
		//var newInpsectionData = [];
		//newInpsectionData.po_no = inspectionData.po_no;
		items.push(inspectionData);
		//console.log(JSON.stringify(items));
		
		DB.single_insert('inspections',items,function(tx,results,insertId){

			if(insertId!=undefined) {
				//console.log("Setting App.page.main.InspectionData.id");
				App.page.main.InspectionData.id = insertId;
				App.page.main.InspectionData.inspection_serial_id = inspectionNum;
				$("h1").css({'color':'#fff'});
			}
			if(done!=undefined) redirect('view.html#'+App.page.main.InspectionData.id);
		});
	}
	
};

