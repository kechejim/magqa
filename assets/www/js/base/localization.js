
var localization = {};
localization.strings  = jsonLang;

localization.initialize = function () {
    console.log('initializing l10n.');

    //added snippet if locale/localStorage('locale') is null
    if(!(this.locale)){
         this.locale = 'en_US';
         Storage.set('locale', 'en_US');
    }

    try{
	    if (this.locale == 'en_US') {
	        //set for locale
	        Storage.set('locale', 'en_US');
	        for (var property in localization.strings) {
	            $('.str_' + property).html(localization.strings[property].en_US);
	        }
	    } else {
	        //set for locale
	        Storage.set('locale', 'zh_CN');
	        for (var property in localization.strings) {
	            $('.str_' + property).html(localization.strings[property].zh_CN);
	        }
	    }    	
    } catch (e) {
    }
};

localization.get = function(key) {
	var locString = "str_" + key + " not found";
    try {
       locString = localization.strings[key][Storage.get('locale')]; 
    } catch (e) {
        console.log ("str_"+key+" not found");
    }
    return locString;
};
