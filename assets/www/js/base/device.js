
_IS_RIPPLE_EMULATOR = parent.$('#browser-inject-js-header').length > 0;

function onDeviceReady(callback) {

	if(_IS_RIPPLE_EMULATOR) cordova.addDocumentEventHandler('backbutton'); 
	document.addEventListener("backbutton", onBackKeyDown, false);
	//on app resume
	document.addEventListener("resume", onResume, false);
	
	try{
		$('[data-role=footer]').css({
			'position': 'fixed !important',
			'left': '0px !important',
			'right': '0px !important',
			'bottom': '0px !important',
			'z-index': '999 !important'
		});	
	} catch(e) {
	}

	$(document).ready(function(){
		console.log("document.ready");

		$('[data-role=listview]').find('a').off('click').on('click',function(e){		
			e.preventDefault();
			
			$('#pop-set-main').panel('close');

			console.log(location.href);
			//var current = location.href.split('#')[1].split('&')[0];
			//if(number(current)>0 || current==''){
				if( !App.page.main.validateMain() ){
					return false;
				}
				//}
			var href = $(this).attr('href');
			
			if(href == '#result'){
				var Factory_NoImages = [];				
				$('#popupResult').off('click').on('click','.continue', function(e){
					$.mobile.changePage(href, {
						transition: 'slide'
					});
				});
				$('#factorycondition').find('.div-factory').each(function(){
					try{
						var photo_container = App.page.main.InspectionData[$(this).attr('id')];
						if(photo_container.photos.length==0){
							Factory_NoImages.push('<strong>'+$(this).find('strong').html()+'</strong>' + ' ' + localization.get('hasnophoto'));
						}
					}catch(e){
					}
				});

				if(Factory_NoImages.length>0){
					$('#popup-items').html(Factory_NoImages.join('<br/>'));
					$.mobile.changePage('#popupResult', {
						transition: 'slide'
					});
					return;
				}
			}
			$.mobile.changePage(href, {
				transition: $(this).data('transition'),
				reverse: $(this).data('direction')
			});

			$(this).closest('[data-role="popup"]').popup('close');
			$.mobile.loading('hide');
		});

		$('.appversion').html(App.version + " " + localStorage.getItem("ws_name"));
		try{
			if(navigator.connection.type != Connection.NONE && !_IS_RIPPLE_EMULATOR) {
				App.isOnline = true;
				$('.connectivity').removeClass('ui-icon-alert').addClass('ui-icon-check').html('<span class="str_nav_online">'+localization.strings.nav_online[Storage.get('locale')]+'</span>');
			}else{
				App.isOnline = false;
			}
		}catch(e){
			App.isOnline = false;
		}
		callback();
	});

}

function onResume(){
	//document.location = "inspections.html";
	console.log("onResume");
	//App.page.main.showStatus();
}

function onBackKeyDown() {
	console.log("onBackKeyDown");
	//App.page.main.showStatus();
}

function getPhoto(source, callback) {
	navigator.camera.getPicture(callback, onFail, { quality: 50, targetWidth:1024,
		destinationType: Camera.DestinationType.DATA_URL,
		sourceType: source });
}

function captureImage(callback) {
	console.log("start captureImage");
	navigator.camera.getPicture(callback, onFail, { quality: 25, targetWidth:1024,
		destinationType: Camera.DestinationType.DATA_URL
	});
}

function getGPSLocation (callback) {
	console.log('get GPS Location');
    navigator.geolocation.getCurrentPosition(callback,    	
		function onError(error) {
			console.log('get GPS Location error : '  + error);
		    //alert('code: '    + error.code    + '\n' + 'message: ' + error.message + '\n');
		}
	);   	    
}

function onFail(message) {
    alert('Capture image failed : ' + message);
}