
DB = { init : null, conn : null };

DB.init = function(){
	DB.conn = window.openDatabase("QCAPPData", "1.0", "QCAPP DB", 10000000);
}

DB.execsql = function(sql, callback){
	function populateDB(tx) {
		tx.executeSql(sql);
	}
	DB.conn.transaction(populateDB, callback, callback);
}

DB.newTable = function(tablename, fields, callback){
	function populateDB(tx) {
		var dropsql = 'DROP TABLE IF EXISTS ' + tablename;
		var createsql = 'CREATE TABLE IF NOT EXISTS '+ tablename +' ('+ fields +')';

		console.log(dropsql);
		console.log(createsql);
		
		tx.executeSql(dropsql);
		tx.executeSql(createsql);
	}
	DB.conn.transaction(populateDB, callback, callback);
}

DB.dropTable = function(tablename, callback){
	function populateDB(tx) {
		tx.executeSql('DROP TABLE IF EXISTS ' + tablename);
	}
	DB.conn.transaction(populateDB, callback, callback);
}

DB.clearTable = function(tablename, callback){
	function populateDB(tx) {
		for(var i = 0; i < tablename.length; i++){
			tx.executeSql('DELETE FROM ' + tablename[i]);
		}
	}
	DB.conn.transaction(populateDB, callback, callback);
}

DB.remove = function(tablename, callback){
	function populateDB(tx) {
		tx.executeSql('DELETE FROM ' + tablename);
	}
	DB.conn.transaction(populateDB, callback, callback);
};

DB.remove2 = function(tablename, where, callback){
	function populateDB(tx) {
		var tmp2 = [];
		for(var k in where){
			if(k != 'po_no' && $.isNumeric( where[k] )){ 
				tmp2.push( k + '=' + where[k] );
			}else{
				tmp2.push( k + '="' + where[k] + '"');
			}
			
		} 
		tx.executeSql('DELETE FROM ' + tablename+ ' WHERE ' + tmp2.toString());
	}
	DB.conn.transaction(populateDB, callback, callback);
};

DB.check = function(tablename, callback){
	function queryDB(tx) {
		tx.executeSql('SELECT * FROM ' + tablename, [], callback, callback);
	}
	DB.conn.transaction(queryDB, callback);
}

DB.insert = function(tablename, data, callback){
	fields = [];
	value = [];
	col = [];
	for(key in data[0]){
		fields.push(key);
		col.push("?");
	}
	for(var i = 0; i < data.length; i++){
		var vals = [];
		for(var a = 0; a < fields.length; a ++){
			//console.log(fields[a]);
			//console.log(data[i][fields[a]]);
			if(fields[a] != 'po_no' && $.isNumeric( data[i][fields[a]] )){
				vals.push(data[i][fields[a]]);
			}else{
				vals.push(data[i][fields[a].toString()]);
			}
		}
		value.push(vals);
	}
	function populateDB(tx){
		for(var i = 0 ; i < value.length ; i++){
			try {
				var sql = 'INSERT INTO '+ tablename +' ('+ fields.toString() +') VALUES ('+ col.toString() +')';
				tx.executeSql(sql, value[i]);
			}catch(e){
				syncLog("DB.insert error " + e);
				syncLog("SQL : " + sql);
			//alert('Err 1 \n' + e);
			}
		}
	}
	DB.conn.transaction(populateDB, callback, callback);
}

DB.single_insert = function(tablename, data, callback){
	fields = [];
	value = [];
	col = [];
	for(key in data[0]){
		fields.push(key);
		col.push("?");
	}
	for(var i = 0; i < data.length; i++){
		var vals = [];
		for(var a = 0; a < fields.length; a ++){
			//console.log(fields[a]);
			//console.log(data[i][fields[a]]);
			if(fields[a] != 'po_no' && fields[a] != 'style' && $.isNumeric( data[i][fields[a]] )){				
				vals.push(data[i][fields[a]]);
			}else{
				vals.push(data[i][fields[a].toString()]);
			}
		}
		value.push(vals);
	}
	function populateDB(tx){
		for(var i = 0 ; i < value.length ; i++){
			//console.log('INSERT INTO '+ tablename +' ('+ fields.toString() +') VALUES ('+ value[i].toString() +')');

			try {
			tx.executeSql('INSERT INTO '+ tablename +' ('+ fields.toString() +') VALUES ('+ col.toString() +')', value[i], 
				function(tx, results){
							var lastInsertId = results.insertId; // this is the id of the insert just performed
							callback(tx,results,lastInsertId);
						}, 
						callback
						);
			}catch(e){
				syncLog("DB.insert error " + e);
				syncLog("SQL : " + sql);
			//alert('Err 1 \n' + e);
			}

		}
	}
	DB.conn.transaction(populateDB, callback, callback);
}

DB.find = function(tablename, fields, callback){
	var qryString = "*";
	if(!(fields.length == 0)){
		qryString = fields.toString();
	}
	function queryDB(tx) {
		tx.executeSql('SELECT '+ qryString +' FROM ' + tablename, [], callback, callback);
	}
	DB.conn.transaction(queryDB, callback);
}

DB.update = function(tablename, where, setfields, callback){
	var tmp = [];
	for(var k in setfields){		
		if(k != 'po_no' && k != 'style' && $.isNumeric( setfields[k] )){
			tmp.push( k + '=' + setfields[k] );
		}else{
			tmp.push( k + '="' + setfields[k]+ '"');
		}

	}
	var tmp2 = [];
	if (where  instanceof Array || typeof where == 'object') {
		for(var k in where){
			if(k != 'po_no' && $.isNumeric( where[k] )){ 
				tmp2.push( k + '=' + where[k] );
			}else{
				tmp2.push( k + '="' + where[k] + '"');
			}			
		}
	} else {
		tmp2.push(where);
	}
	function queryDB(tx) {
		tx.executeSql('UPDATE ' + tablename + ' SET ' + tmp.toString() + ' WHERE ' + tmp2.toString() , [], callback, callback);
	}
	DB.conn.transaction(queryDB, callback);
}

//initialize database
DB.init();
