
var Storage = {
	set : function(key, val){
		var data = typeof val=='object' ? JSON.stringify(val) : val;
		window.localStorage.setItem(key, data);
	},
	get : function(key){
		var g;
		try{
			g = JSON.parse(window.localStorage.getItem(key));
		} catch(e){
			g = window.localStorage.getItem(key);
		}
		return g;
	},
	rem : function(key){
		window.localStorage.removeItem(key);
	}
};

var waitForFinalEvent = (function () {
	var timers = {};
	return function (callback, ms, uniqueId) {
		if (!uniqueId) {
			uniqueId = "Don't call this twice without a uniqueId";
		}
		if (timers[uniqueId]) {
			clearTimeout (timers[uniqueId]);
		}
		timers[uniqueId] = setTimeout(callback, ms);
	};
})();

function getCurrentDate() {
	var d = new Date();
	var y = d.getFullYear(); 
	var dd = d.getDate();
	var mm = d.getMonth()+1; 
	if(dd < 10){ dd = '0' + dd };
	if(mm < 10){ mm = '0' + mm }; 
	return y + '-' + mm + '-' + dd;
}

function getCurrentDateTime() {
	var d = new Date();
	var y = d.getFullYear(); 
	var dd = d.getDate();
	var mm = d.getMonth()+1; 
	if(dd < 10){ dd = '0' + dd };
	if(mm < 10){ mm = '0' + mm }; 
	var h = d.getHours();
	var m = d.getMinutes();
	var s = d.getSeconds();
	if(h < 10){ h = '0' + h }; 
	if(m < 10){ m = '0' + m }; 
	if(s < 10){ s = '0' + s }; 
	return y + '-' + mm + '-' + dd + ' ' + h + ':' + m + ':' + s;
}

function clearSyncLog() {
	$("#synclog").html("");
}

function syncLog(log) {
	console.log(log);
	$("#synclog").append(log);
	$("#synclog").append("<br>");
}

function factoryOnClick(ndx){
	var item = factoryItems[ndx];
	
	$('#po-text-factory').val(item.factory);
	$('#text-factory_contacts_emailaddress').val(item.email);
	$('#text-factory_contacts_fullname').val(item.contact);

	App.page.main.save();

	$.mobile.changePage('#main',{
		transition: 'slide',
		reverse: 'true'
	});
}

var cool = function(obj){
	return obj !== undefined && obj != null;
};

var clone = function(obj){
	if(obj == null || typeof(obj) != 'object')
		return obj;
	var temp = new obj.constructor(); 
	for(var key in obj){
		temp[key] = clone(obj[key]);
	}
	return temp;
};

var getTypeName = function(obj) {
	if (obj == null) {
		return 'null';
	} else if ($.isArray(obj)) {
		return 'array';
	} else {
		return typeof(obj);
	}
}

var cloneOmitObject = function(obj){
	if (getTypeName(obj) == 'array') {
		 return 'array[' + obj.length + ']';
	}
	if(obj == null || typeof(obj) != 'object') {
		return obj;
	}
	var temp = new obj.constructor(); 
	for(var key in obj){
		temp[key] = cloneOmitObject(obj[key]);
	}
	return temp;
};

var addslashes = function(str) {
  return (str + '')
  .replace(/[\\"']/g, '\\$&')
  .replace(/\u0000/g, '\\0');
};

var getNext3 = function(number) {
	return number<3 ? number : getNext3(number-3);
};

var check = function(o){
	return o == undefined || typeof o != 'object';
};

var number = function(n){
	n = new Number(n);
	return parseInt(n) == 'NaN' || typeof n == undefined ? 0 : parseInt(n);
};

var redirect = function(url){
	DB.check('inspections', function(){
		location.href = url;
	});
};

var validateEmail = function ($email) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	if( !emailReg.test( $email ) ) {
		return false;
	} else {
		return true;
	}
};

var myalert = function(title, msg, callback){
	var dialog = $('#popupAlert');
	dialog.popup('open');
	dialog.find('.title').html(title);
	dialog.find('.ui-content-description').html(msg);			
	
	dialog.find('.btn_yes').off('click').on('click', callback);
};

function rotateBase64Image(base64data, callback) {
	
	var c 	=	document.getElementById('hiddencanvas');
	var ctx =	c.getContext("2d");
	var img =	new Image();
	$(img).attr('src', base64data);

	img.onload = function(){
		c.width  = img.height;
		c.height = img.width;
		
		ctx.rotate(90* Math.PI / 180);
		ctx.drawImage(img, 0, -img.height);
		
		callback (c.toDataURL());
	};
};

/*var api_urls = {
	getData : null,
	login : null,
	inspector_login : null,
	process : false,
	postdata : null,
	appdataSync : null,
	userkey : 'qciapp-QWERTY-key1910-userdata',
	appdata : 'qciapp-QWERTY-key1910-appdata',
	signs : 'qciapp-QWERTY-key1910-signdata',
	session : 'qciapp-QWERTY-key1910-sessiondata'
};*/

var ws_url = localStorage.getItem("ws_url");
if (ws_url == null)
{
	localStorage.setItem("ws_url", "http://203.198.58.104/wstest/request"); 
	localStorage.setItem("ws_name", "TEST"); 
}

var clickTrim = function(id) {
	App.page.main.InspectionData.trim = {};
	$('.trimlabel').each(function(index) {
		var triminput = $(this.parentNode).find('input');
		var trimid = triminput.attr('id');
		var trimtext = $(this).text();

		console.log(trimtext);
		
		var trimtype = $(this).attr('data-trimtype');

		if (trimtype == "B") {
			var trimselected = triminput.attr('data-cacheval');
			var trimchecked = triminput.attr('checked');
			App.page.main.InspectionData.trim[trimid] = {};
			if (trimselected == "false" || trimchecked == "yes") {
				App.page.main.InspectionData.trim[trimid]['checked']='yes';
				//console.log( index + ": (" + trimid + ")" + trimtext + ":" + trimselected + ":" + trimchecked);
			} else {
				App.page.main.InspectionData.trim[trimid]['checked']='no';
			}
			
			App.page.main.InspectionData.trim[trimid]['type'] = trimtype;
			App.page.main.InspectionData.trim[trimid]['desc_en'] = $(this).attr('data-desc-en');
			App.page.main.InspectionData.trim[trimid]['desc_cn'] = $(this).attr('data-desc-cn');
					
		}

		if (trimtype == "I") {
			var intval = triminput.val();
			
			//console.log( index + ": (" + trimid + ")" + trimtext + ":" + intval);
			App.page.main.InspectionData.trim[trimid] = {};
			App.page.main.InspectionData.trim[trimid]['type'] = trimtype;
			App.page.main.InspectionData.trim[trimid]['desc_en'] = $(this).attr('data-desc-en');
			App.page.main.InspectionData.trim[trimid]['desc_cn'] = $(this).attr('data-desc-cn');
			App.page.main.InspectionData.trim[trimid]['val'] = intval;	

		}

	  	$(this).css("background", "#f6f6f6");
	});
	console.log(JSON.stringify(App.page.main.InspectionData.trim));
	App.page.main.save();
}

var selectDefect = function(item) {
	$(".defect_points").removeClass("row_selected");
	//$(".defect-select-row").attr('checked', false);
	$(item.parentNode.parentNode.parentNode.parentNode.parentNode.parentNode).addClass("row_selected");
	//$(item).find(".defect-select-row").attr('checked', true);
	//console.log($(item).find(".defect-select-row"));
}
var selectFactoryCondition = function(item) {
	$(".div-factory").removeClass("row_selected");
	$(item).addClass("row_selected");
}

var removeFactoryPhoto = function (id,photo_id) {
	var dp = App.page.main.InspectionData['factory_photos'];
	console.log(dp.length + ":"+ photo_id);

	var photos = App.page.main.InspectionData.factory_photos;
	$('#imagePopup img').attr('src', photos[photo_id]);
	$.mobile.changePage('#imagePopup');

	var measurements_photos = $('#measurement').find('.measurement-photos');

	$('#btn-confirm-delete-photo').hide();
	$('#btn-delete-photo').off('click').on('click', function(e){
		$('#btn-confirm-delete-photo').show();
	});
	$('#btn-confirm-delete-photo').off('click').on('click', function(e){
		App.page.main.InspectionData.factory_photos.splice(photo_id,1);
		var container = null;
		container = measurements_photos;
		container.html("");
		try{
			var photos = App.page.main.InspectionData.factory_photos;
			console.log("Photo Length:"+ photos.length);
			if(photos.length>0){
				numFactoryPhotos = photos.length;
				var i=0;
				$.each(photos, function(k,v){
					console.log("Photo:"+ i);
					container.append('<div class="ui-bar" id="factoryphoto-'+i+'" onclick="removeFactoryPhoto(this,'+i+')"><img src="'+ v +'"> </div>');
					i++;
				});
			}
		}catch(e){
		}

		$.mobile.back();
	});

	$('#btn-rotate-photo').off('click').on('click', function(e){
		
		rotateBase64Image(photos[photo_id], function (rotatedData){ 
		
			App.page.main.InspectionData.factory_photos[photo_id] = rotatedData;			
			
			$('#imagePopup img').attr('src', rotatedData);
		});		

	});

}

var removePhoto = function (colorsize,photo_id,id) {
	console.log("removePhoto "+ ":" + colorsize + ":" + photo_id);

	if (colorsize.indexOf("__") >= 0) {
		var csid = colorsize.split('__')[0];
		var cat = colorsize.split('__')[1];
		var subcat = colorsize.split('__')[2];
		var dp = App.page.main.InspectionData.colorsize_defect_points[csid];

		$('#btn-confirm-delete-photo').hide();
		$('#btn-delete-photo').off('click').on('click', function(e){
			$('#btn-confirm-delete-photo').show();
		});
		$('#btn-confirm-delete-photo').off('click').on('click', function(e){
			dp[cat]['defectpoints'][subcat]['photos'].splice(photo_id,1);
			id.parentNode.innerHTML = "";
			try{
				if(dp[cat]['defectpoints'][subcat]['photos'].length>0){
					$.each(dp[cat]['defectpoints'][subcat]['photos'],function(k,v){
						add_photos($('#'+colorsize),v);
					});
				}
			}catch(e){
			}
			$.mobile.back();
		});

		$('#btn-rotate-photo').off('click').on('click', function(e){
		
			rotateBase64Image(dp[cat]['defectpoints'][subcat]['photos'][photo_id], function (rotatedData){ 
			
				App.page.main.InspectionData.colorsize_defect_points[csid][cat]['defectpoints'][subcat]['photos'][photo_id] = rotatedData;	
				$('#imagePopup img').attr('src', rotatedData);

				id.parentNode.innerHTML = "";
				try{
					if(dp[cat]['defectpoints'][subcat]['photos'].length>0){
						$.each(dp[cat]['defectpoints'][subcat]['photos'],function(k,v){
							add_photos($('#'+colorsize),v);
						});
					}
				}catch(e){
				}
			});		

		});

		$('#imagePopup img').attr('src',dp[cat]['defectpoints'][subcat]['photos'][photo_id]);
		$.mobile.changePage('#imagePopup');
	} else {
		var fc = App.page.main.InspectionData[colorsize];
		$('#btn-confirm-delete-photo').hide();
		$('#btn-delete-photo').off('click').on('click', function(e){
			$('#btn-confirm-delete-photo').show();
		});
		$('#btn-confirm-delete-photo').off('click').on('click', function(e){
			fc['photos'].splice(photo_id,1);
			id.parentNode.innerHTML = "";
			try{
				if(fc['photos'].length>0){
					$.each(fc['photos'],function(k,v){
						add_photos($('#'+colorsize),v);
					});
				}	
			}catch(e){
			}
			$.mobile.back();
		});
		$('#btn-rotate-photo').off('click').on('click', function(e){
		
			rotateBase64Image(fc['photos'][photo_id], function (rotatedData){ 
				App.page.main.InspectionData[colorsize]['photos'][photo_id] = rotatedData;	
				$('#imagePopup img').attr('src', rotatedData);

				id.parentNode.innerHTML = "";
				try{
					if(fc['photos'].length>0){
						$.each(fc['photos'],function(k,v){
							add_photos($('#'+colorsize),v);
						});
					}	
				}catch(e){
				}
			});		

		});
		$('#imagePopup img').attr('src', fc['photos'][photo_id]);
		$.mobile.changePage('#imagePopup');
	}
}

var add_photos = function(container, img){
	try{
		//console.log(container);
		var block = ['a','b','c'];
		var photo_container = container.find('.photos');
		var id = $(container).attr('id');
		// if(photo_container.length==0){
			// photo_container = $('<div class="ui-grid-b photos"></div>');			
			// $(photo_container).insertAfter(container);
		// }
		var photo_id = photo_container.find('img').length; // do we need this jason? i think not!
		//photo_container.append($('<div class="ui-block-'+block[photo_id]+'"><div class="ui-bar"><a href="#imagePopup"><img id = "'+id+'-'+photo_id+'" src="data:image/png;base64,'+img+'"></a></div></div>'));
 
		//photo_container.append($('<div class="ui-block-'+block[photo_id]+'"><div class="ui-bar"><a href="#imagePopup" data-rel="popup" data-position-to="window" ><img id = "'+id+'-'+photo_id+'" src="data:image/png;base64,'+img+'"></a></div></div>'));


		photo_container.append($('<div onclick="removePhoto('+"'"+id+"',"+photo_id+',this)" class="ui-block-'+block[photo_id]+'"><div class="ui-bar"><img id = "'+id+'-'+photo_id+'" src="'+img+'"></div></div>'));
		App.page.main.save();
	}catch(e){
		// alert(e);
	}
};

var initFactoryPhotos = function(id){		
	if(check(App.page.main.InspectionData[id])){
		App.page.main.InspectionData[id] = {};
	}
	if(check(App.page.main.InspectionData[id].photos)){
		App.page.main.InspectionData[id].photos = [];
	}
};

