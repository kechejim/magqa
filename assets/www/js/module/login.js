//for login page
App.page.login = {};

App.page.login.init = function(){		

	$('#txt-username').val(Storage.get('username'));
	
	localization.locale = Storage.get('locale');
	localization.initialize();
	
	$('#btn-login').off('click').on('click', function(e){
		e.preventDefault();
		//if($.trim($('#txt-username').val())=='' || $.trim($('#txt-password').val())==''){
		//	$('#pop-message > p').html(localization.get('dlg_loginfail'));
		//	$('#pop-message').popup('open');
		//	return;
		//}
		$.mobile.loading('show');

		DB.find("inspectors WHERE username = '" + $('#txt-username').val() + "' and password = '" + $('#txt-password').val() + "'", [], function(err, results){
		//DB.find("inspectors WHERE username = '" + $('#txt-username').val() + "'", [], function(err, results){

		if(err.code){ 
			$('#pop-message > p').html(localization.get('dlg_loginfail'));
			$('#pop-message').popup('open');
		}
		if(results.rows){
			if( results.rows.length == 1 ){

				Storage.set('user-login', results.rows.item(0));
					//console.log(Storage.get('user-login'));
					$('#pop-message > p').html(localization.get('dlg_loginsuccess'));
					$('#pop-message').popup('open');
					document.location = 'home.html';
				} else {
					$('#pop-message > p').html(localization.get('dlg_loginfail'));
					$('#pop-message').popup('open');
				}
			}else{
				Storage.rem('user-login');
			}
			$.mobile.loading('hide');
		});
	});

	$('#btn-cancel').off('click').on('click', function(e){
		e.preventDefault();
		setTimeout(function(){
			navigator.app.exitApp();
		},100);
	});

	$('#menu').off('pageshow').on('pageshow', function(e){
		$('.language').off('click').on('click', function(e){
			var lang = $(this).data('lang');
			Storage.set('locale', lang);
			localization.locale = lang;
			localization.initialize();
			$.mobile.changePage('#login', {
				transition: 'slide',
				reverse: 'true'
			});
		});
	});

	$('#link-changeuser').off('click').on('click', function(e){
		
		/*$('#link-login-admin').off('click').on('click', function(e){
			
			var pw = $('#admin-password').val();

			if($.trim(pw)=='qcappadmin'){
				setTimeout(function(){
					$('#admin-password').val('');
					$.mobile.changePage('#changeuser',{
						transition:'slide',
						reverse: 'true'
					});
				},100);
			} else {
				$('#admin-password').val('');
				alert(localization.get('dlg_admindenied'));
			}

		});

		$('#popupDialog').popup('open');*/

		setTimeout(function(){
					$('#admin-password').val('');
					$.mobile.changePage('#changeuser',{
						transition:'slide',
						reverse: 'true'
					});
				},100);
	});


	$('#updatedb').off('pageshow').on('pageshow', function(e){
		App.OnSyncPageShow();
	});


	$('#link-updatedb').off('click').on('click', function(e){
		App.OnDoUpdate();
	});


	$('#changeuser').off('pageshow').on('pageshow', function(e){
		var li = $('.select-user');
		if(li.length>1) return;
		var ul = li.parent();
		$('.select-user').remove();
		DB.find('inspectors',[],function(e,r){
			if(e){
					//console.log(e);
				}
				if(r.rows){
					for(i=0;i<=r.rows.length-1;i++){
						var table = li.clone();
						var items = r.rows.item(i);
						var locale = ['en_US','zh_CN'];
						$.each(items, function(k,v){
							table.find('.'+k).html(v);
						});
						table.attr('id', items.id);
						ul.append(table);
					}
				}
			});

		$('#changeuser').off('click').on('click', '.select-user', function(e){
			var email = $(this).find('.email').html();
			var locale = $(this).find('.lang').html();
			var username = $(this).find('.username').html();
			var userename = $(this).find('.name').html();
			var usercname = $(this).find('.cname').html();

			localization.locale = locale;
			localization.initialize();

			$('#txt-username').val(username);
			Storage.set('email',email);
			Storage.set('username',username);
			Storage.set('usercname',usercname);
			Storage.set('locale',locale);
			$.mobile.changePage('#login',{
				transition: 'slide',
				reverse: 'true'
			});
		});


	});

	/* Tony Add for Server URL */
	$('#link-update-server-url').off('click').on('click', function(e){
		var ws_url = localStorage.getItem("ws_url");
		var ws_name = localStorage.getItem("ws_name");

		$('#server-url').val(ws_url);
		$('#server-name').val(ws_name);

		$('#popupServerURL').popup('open');

		$('#btn-save-server-url').off('click').on('click', function(e){	
			try{
				if(navigator.connection.type != Connection.NONE && !_IS_RIPPLE_EMULATOR) {
					App.isOnline = true;
					$('.connectivity').removeClass('ui-icon-alert').addClass('ui-icon-check').html('<span class="str_nav_online">'+localization.strings.nav_online[Storage.get('locale')]+'</span>');
				
					if (ws_url != $('#server-url').val()) {
						localStorage.setItem("ws_url", $('#server-url').val());
						localStorage.setItem("ws_name", $('#server-name').val());
						Storage.set('version',0);
						setTimeout(function(){
							document.location = "login.html#updatedb";
						},500);	
					}
				
				}else{
					alert(localization.get("retryonline"));
					App.isOnline = false;
				}
			}catch(e){
				alert(localization.get("retryonline"));
				App.isOnline = false;
			}


		});
	});
	/* END Tony Add for Server URL */
		

};