//for signing page
App.page.signing = {};

App.page.signing.init = function(){

	console.log("signing");
	
	localization.locale = Storage.get('locale');
	localization.initialize();
	
	var inspector_sign = null;
	var factory_sign = null;
	
	var inspection_id = location.href.split('#')[1];
	
	$.mobile.loading('show');
	DB.find('inspections where id='+ inspection_id ,['inspector_sign','factory_sign'], function(err,results){
		$.mobile.loading('hide');
		
		$('#cancel_signing').off('click').on('click', function(e){
			e.preventDefault();
			var dialog = $('#popupDialog');
			dialog.popup('open');
			dialog.find('.title').html(localization.get('cancel_signing'));
			dialog.find('.ui-content-description').html(localization.get('cancel_signing'));			
			dialog.find('.btn_no').off('click').on('click', function(e){
				
			});
			
			dialog.find('.btn_yes').off('click').on('click', function(e){
				redirect('view.html#'+inspection_id);
			});
			$('#signing').page('destroy').page();
		});

		$('#submit_signing1').off('click').on('click', function(e){
			var updateObj = {};
			updateObj['inspector_sign'] = escape(inspector_sign.jSignature('getData', 'image'));
			updateObj['factory_sign'] = escape(factory_sign.jSignature('getData', 'image'));

			if(App.isOnline){
				updateObj['status'] = 3;
				DB.update('inspections', { id : inspection_id }, updateObj , function(err, results){
					if(err.code){ console.log(err); }
					else{
						myalert('<span class="ui-btn-icon-notext ui-icon-check"></span>',localization.get('signaturesaved'),function(){
							document.location = "login.html#updatedb";
						});
//						setTimeout(function(){
//							location.href = "login.html#updatedb";
//						},2000);	
					}
				});
			} else {
				updateObj['status'] = 1;
				DB.update('inspections', { id : inspection_id }, updateObj , function(err, results){
					myalert('<span class="ui-btn-icon-notext ui-icon-check"></span>',localization.get('retryonline'),function(){
						document.location = "home.html";
					});
				});
			}
		});
		
		$('#sign_only').off('click').on('click', function(e){
			e.preventDefault();
			var updateObj = {};
			updateObj['inspector_sign'] = escape(inspector_sign.jSignature('getData', 'image'));
			updateObj['factory_sign'] = escape(factory_sign.jSignature('getData', 'image'));
			updateObj['status'] = 1;
			DB.update('inspections', { id : inspection_id }, updateObj , function(err, results){
				if(err.code){
					alert(err.message);
				}else{
					myalert('<span class="ui-btn-icon-notext ui-icon-check"></span>',localization.get('signaturesaved'),function(){
						document.location = "view.html#"+inspection_id;
					});
				}
			});
		});
		
		$(document).on('click', '.link-home', function(e){
			e.preventDefault();
			App.page.main.save(false,true);

//			setTimeout(function(){
//				location.href = 'home.html';
//			},1000);
		});
		try{
			// $('#inspector').signaturePad({drawOnly:true}).regenerate(unescape(results.rows.item(0).inspector_sign));	
			inspector_sign = $( "#inspector_sign" ).jSignature({'UndoButton':true});
			inspector_sign.jSignature("setData", "data:" + unescape(results.rows.item(0).inspector_sign));
		}catch(e){
				// alert(e);
		}

		try{
			// $('#manufacturer').signaturePad({drawOnly:true}).regenerate(unescape(results.rows.item(0).factory_sign));	
			factory_sign = $( "#factory_sign" ).jSignature({'UndoButton':true});
			factory_sign.jSignature("setData", "data:" + unescape(results.rows.item(0).factory_sign));
		}catch(e){
				// alert(e);
		}
		$('#inspector_clear').off('click').on('click', function(){
			inspector_sign.jSignature('reset');
		});
		$('#factory_clear').off('click').on('click', function(){
			factory_sign.jSignature('reset');
		});
		$('#signpage').page('destroy').page();
		// $('.sigPad').signaturePad({drawOnly:true});
	});
};
