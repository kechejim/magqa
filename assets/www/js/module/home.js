//for home page
App.page.home = {};
App.page.home.init = function(){
	console.log("Home Init");

	localization.locale = Storage.get('locale');
	localization.initialize();
	
	var user =   Storage.get('user-login') ;
	$('#home').off('click').on('click', '#link-new-inspection', function(e){
		e.preventDefault();
		location.href = 'main.html';
	});
	
	$('#link-logout').off('click').on('click', function(e){
		e.preventDefault();
		Storage.rem('user-login');
		location.href = 'login.html';
	});

	$('#link-reset').off('click').on('click', function(e){
		e.preventDefault();
		var updateObj = {};
		var currentStatus = Storage.get("currentStatus");
		if (currentStatus >= 1) {
			updateObj['status'] = 0;
					
		} else {
			updateObj['status'] = 1;
		};
		Storage.set("currentStatus",updateObj['status']);
		console.log("Setting curentStatus to "+Storage.get("currentStatus"));
												
		DB.update('inspections', 'status < 9', updateObj , function(err, results){
			if(err.code){ console.log(err); }
			else{
				location.href = 'login.html#updatedb';
			}
		});
	});

	var color = ['qcinprogress','qcsigned','qcsubmitted','qcsending'];
	var block = ['a','b','c'];
	var counter = 1;

	var fields = ['ins.id','inspection_serial_id','factory_contacts_fullname','ins.po_number','inspection_date','st.name stylename','status','f.name as factoryname'];


	//var sql = 'inspections left join po on cast(po.po_no as text) = cast(inspections.po_no as text) and cast(po.style as text) = cast(inspections.style as text) ';
	var sql = 'inspections order by status, inspection_date desc';

	var select = ['inspections.*'];
	/*
	select.push('po.fibercontent as fibercontent');
	select.push('po.garmentweight as garmentweight');
	select.push('po.shipmode as shipmode');
	select.push('po.shipdate as shipdate');
	select.push('po.vendor as vendor');
	select.push('po.factory as factory');
	select.push('po.qty as qty');
	select.push('po.styledesc as styledesc');
	select.push('po.destination as destination');
	select.push('po.producttype as producttype');
	*/
	
	$('#inspection-report').append('<div class="ui-block-a"><a id="link-new-inspection" href="#"><div class="qcboxes qcnew"><h4>查驗報告</h4><h3>Inspection Report</h3><h1 class="gray">新增</h1><h2 class="gray">NEW</h2></div></a></div>');

	DB.find(sql,select,function(err, results){

		for(i=0;i<=results.rows.length-1;i++){
			var field = results.rows.item(i);
			//console.log(field);
			if(counter>=3) counter = 0;
			var color_status = color[field.status] == undefined ? color[0] : color[field.status];
			var report = [];
			
			var imagesrc = field.tileimage;
			if(imagesrc === "undefined") {
				imagesrc = 'images/icon-shirt.png';
			}
			
	        report.push('<div class="ui-block-'+block[counter]+'">');
	        report.push('<a href="view.html#'+field.id+'">');
	        report.push('	<div class="qcboxes '+color_status+'">');
	        report.push('		<h4>查驗報告</h4>');
	        report.push('		<h3>Inspection Report</h3>');
	        report.push('		<p class="gray">'+field.inspection_serial_id+'</p>');
	        report.push('		<p><img src='+ imagesrc + ' width=100 height=100></p>');
	        report.push('		<p class="blue">'+field.firm + " " + field.inspection_date+'</p>');
	        report.push('		<p class="blue">Cust: '+field.customer+'</p>');
	        report.push('		<p class="blue">PO #: '+field.po_no.substring(0,30)+'</p>'); 
	        report.push('		<p class="blue">Style #: '+field.style+'</p>');
	        report.push('		<p class="blue">'+ field.inspector+'</p>');
	        report.push('		<p class="blue">Factory: '+field.factory+'</p>');			        
	        report.push('	</div>');
	        report.push('</a>');
	        report.push('</div>');
	        
	        $('#inspection-report').append(report.join(''));
	        counter ++;
	    }

    	$('#home').page('destroy').page();	
	});

	$(document).off('click').on('click', '.gotoview', function(e){
		e.preventDefault();
		location.href = $(this).attr('href');
	});

};

