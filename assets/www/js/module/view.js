//for view page
App.page.view = {};

var addPhotos = function(imgSource,prefix){
	var container = $('#photos');
	var allphotos = container.find('.IMG_'+prefix).length+1;
	var tr = container.find('tr').last();
	var tdcount = tr.find('td').length;
	if(tr.length==0){
		container.append('<tr></tr>');
		tr = container.find('tr').last();
		tdcount = 0;
	}
	if(tdcount==3){
		container.append('<tr></tr>');
		tr = container.find('tr').last();
		tdcount = 0;
	}
	tr.append('<td class="IMG_'+prefix+'" id="'+prefix+allphotos+'">'+prefix+allphotos+'<br/><img style="height:auto;width:200px;" src="'+imgSource+'" /></td>');
	return prefix+allphotos;
}


App.page.view.init = function(){

	console.log("View Init");
	var photos_super_global = [];
	var x = 0;
	
	$.mobile.loading('show');
	$('.lang-en').off('click').on('click', function(e){
		Storage.set('locale', 'en_US');
		localization.locale = 'en_US';
		localization.initialize();
	});
	
	$('.lang-ch').off('click').on('click', function(e){
		Storage.set('locale', 'zh_CN');
		localization.locale = 'zh_CN';
		localization.initialize();
	});
	
	localization.locale = Storage.get('locale');
	localization.initialize();
	
	var inspection_id = location.href.split('#')[1];
	
	console.log("Make Report");
	// hardcoded
	var report_status = ["Accepted", "Re-Inspection", "Hold Shipment"];
	var qc_status = ["Accepted", "Rejected"];
	var inspections = [localization.get('inlineinspection'),localization.get('finalinspection')];
	var shipmode = ['Sea','Air','Truck'];
	
	var item;
	var join = [];
	//join.push('left join po on cast(po.po_no as text) = cast(inspections.po_no as text) and cast(po.style as text) = cast(inspections.style as text) ');

	
	var select = ['inspections.*'];
	/*
	select.push('po.fibercontent as fibercontent');
	select.push('po.garmentweight as garmentweight');
	select.push('po.shipmode as shipmode');
	select.push('po.shipdate as shipdate');
	select.push('po.vendor as vendor');
	select.push('po.factory as factory');
	select.push('po.qty as qty');
	select.push('po.styledesc as styledesc');
	select.push('po.destination as destination');
	select.push('po.producttype as producttype');
*/

	var sql = 'inspections '+join.join(' ') +' WHERE inspections.id = ' + inspection_id + '';

	DB.find(sql, select, function(err, results) {
		console.log("Got Inspections");


		if(err){ 
		}
		if(results.rows){
			item = results.rows.item(0);
			//console.log(JSON.stringify(item));
			if(item.status!=0){
				$('#link-change-update,#link-sign-report,#link-delete-report').addClass('ui-state-disabled');
				if(item.status==3){
					$('#link-sign-report').removeClass('ui-state-disabled');
				}
			}
			if(item.status!=1){
				$('#submit_signing').addClass('ui-state-disabled');
			}
			try{
				var sign = unescape(item.inspector_sign);
				if(sign === "null"){
					$('#inspector_sign').attr('src', 'images/sign-nf.jpg');	
				} else {
					$('#inspector_sign').attr('src', 'data:'+sign);	
				}
			}catch(e){
				alert(e);
			}
			try{
				var fsign = unescape(item.factory_sign);
				if(fsign === "null"){
					$('#factory_sign').attr('src', 'images/sign-nf.jpg');	
				} else {
					$('#factory_sign').attr('src', 'data:'+fsign);
				}
			}catch(e){
				alert(e);
			}
			try{
				var timg = unescape(item.tileimage);
				if(timg === "undefined"){
					$('#tileimage').attr('src', 'images/icon-shirt.png');	
				} else {
					$('#tileimage').attr('src', timg);
					//$('#tileimage').fakecrop({fill: true, wrapperWidth: 250, wrapperHeight: 250});
				}
			}catch(e){
				alert(e);
			}
			$.each(item, function(k,v){
				//console.log("  - Setting item " + k + ":" + v);
				try{
					if($('.view-'+k).length!=0){
						if(k=='inspection_status'){
							v = report_status[v-1];
						}else if(k=='qc_status'){
							v = qc_status[v-1];
						}else if(k=='inspectiontype'){
							v = inspections[v-1];
						}else if($.trim(v)==''){
							v = '<span style="color:red">-</span>';
						}
						$('.view-'+k).html(v);
					}
					var status = ['Accepted','Rejected'];
					if(k=='colorsize_defect_points'){
						try{
							var defectpoints = $.parseJSON(unescape(v));
							
							//console.log(unescape(v));
							
							$.each(defectpoints, function(key,val){
//								console.log(JSON.stringify(key));
//								console.log(JSON.stringify(val));
								
								var sorted=[];								
								var html = [];
								html.push('<tr>');
								var totalDefects = 0;
								var defects_html = [];
								$.each(val, function(a,b){
									if(!check(b)){
										$.each(b.defectpoints, function(dk,dv){
											sorted.push(dv);
										});					
									}
								});
								sorted = sorted.sort(function(a, b) {
									return (parseInt(a['seqno'],10) > parseInt(b['seqno'],10));
								});

								
							    for (var k in sorted) {
							    	var dv = sorted[k];
									if(number(dv.minor_qty) > 0 || number(dv.major_qty) > 0){
										if(totalDefects>0&&defects_html[defects_html.length-1]=='</tr>') defects_html.push('<tr>');
										defects_html.push('<td>'+dv.description+'&nbsp;<font size=-2 style="color:#ddd">['+dv.seqno+']</font></td>');
										defects_html.push('<td>'+localization.get('lbl_minor')+':'+dv.minor_qty+'<br>'+localization.get('lbl_major')+':'+dv.major_qty+'</td>');
										defects_html.push('<td>'+dv.minor_remarks+'<br>'+dv.major_remarks+'</td>');
										var total_images = 0;
										var pref = [];
										try{
											total_images = dv.photos.length;
											$.each(dv.photos, function(pk,pv){
												pref.push(addPhotos(pv,item.inspection_serial_id+'-D'));
											});
										}catch(e){
											// alert(e);
										}
										defects_html.push('<td>'+total_images+'</td>');
										defects_html.push('<td>'+pref.join(',')+'</td>');
										defects_html.push('</tr>');
										totalDefects++;
									}
							    }
								
								
								html.push('<td rowspan="'+totalDefects+'">'+val.colorsize+'</td>'+defects_html.join(''));
								html.push('<tr>');
								html.push('<td class="second">'+localization.get('short_po_qty')+':'+val.quantity+'</td>');
								html.push('<td class="second">'+localization.get('short_completed_qty')+':'+val.completed_quantity+'</td>');
								html.push('<td class="second">'+localization.get('short_inspected_qty')+':'+val.inspected_quantity+'</td>');
								html.push('<td class="second" style="text-align: right;">'+localization.get('short_rejected_qty')+'</td>');
								html.push('<td class="second">'+localization.get('lbl_minor')+':'+val.rejected_quantity_minor+'</td>');
								html.push('<td class="second">'+localization.get('lbl_major')+':'+val.rejected_quantity_major+'</td>');
								html.push('</tr>');
								$('#defect_points').append(html.join(''));
								});
						}catch(e){							
						}
					}
					if($('.view-'+k+'-status').length!=0){
						var field = $.parseJSON(unescape(v));
						try{
							$('.view-'+k+'-status').html(status[field.status-1]);
						} catch(exception){
						}
						try{
							$('.view-'+k+'-remarks').html(field.remarks);
						} catch(exception){
						}
						try{
							$('.view-'+k+'-photos').html(field.photos.length);
							var pref = [];
							$.each(field.photos, function(pk,pv){
								//pref.push(addPhotos('data:image/png;base64,'+pv,item.inspection_serial_id+'-F'));
								pref.push(addPhotos(pv,item.inspection_serial_id+'-F'));

							});
							$('.view-'+k+'-ref').html(pref.join(','));							
						} catch(exception){
							$('.view-'+k+'-photos').html('0');
						}
					}
					if(k=='factory_photos'){
						var photos = $.parseJSON(unescape(v));
						try{
							$('.measurement_photos').html(photos.length);
							var pref = [];
							$.each(photos, function(pk,pv){
								//pref.push(addPhotos('data:image/png;base64,'+pv,item.inspection_serial_id+'-M'));
								pref.push(addPhotos(pv,item.inspection_serial_id+'-M'));
							});
							$('.measurement_photos_ref').html(pref.join(','));
						}catch(e){
							$('.measurement_photos').html('0');
						}
					}
					if (k=='trim'){
						var trims = $.parseJSON(unescape(v));
						var html = "";
						var nItems = 0;
						$.each(trims, function(pk,pv){
							if (nItems % 2 == 0) {
								html += "<tr>";
							}
							if (pv.type == "B") {
								html += "<td>";
								html += '<span style="width:50px;display: inline-block;">';
								if (pv.checked=='yes') {
									html += '<input type="checkbox" checked>';
								} else {
									html += '<input type="checkbox">';
								}
								html += '</span><span style="display: inline-block;">';
								html += pv.desc_en;
								html += "<br>" + pv.desc_cn  + "</span></td>";
							}
							if (pv.type == "I") {
								html += '<td><font color="blue" size="+1"><strong>' + pv.val + '</strong></font>&nbsp;';
								html += pv.desc_en + "<br>";
								html += '<font color="blue" size="+1"><strong>' + pv.val + '</strong></font>&nbsp;';
								html += pv.desc_cn + "</td>";
							}
							if (nItems % 2 == 1) {
								html += "</tr>";
							}
							nItems++;
						});
						if (nItems % 2 == 1) {
							html += "<td></td></tr>";
						}

						$('.trim_body').html(html);
					}
				}catch(e){
				}
			});
			console.log("Retrieving PO");
			DB.find("po where cast(po_no as text)='" + item.po_number + "'",[],function(err2, r){
				if(r.rows){
					if(r.rows.length>0){
						//console.log(r.rows.item(0));
						$.each(r.rows.item(0), function(k,v){
							if($('.view-'+k).length!=0){
								if(k=='shipmode_id'){
									v = shipmode[v-1];
								}
								if($.trim(v)==''){
									v = '<span style="color:red">-</span>';
								}
								$('.view-'+k).html(v);
							}
						});
					}
				}
				$.mobile.loading('hide');
			});
		}
	});

	$( "#popupPhotoLandscape" ).on({
		popupbeforeposition: function() {
			var maxHeight = $( window ).height() - 60 + "px";
			$( "#popupPhotoLandscape img" ).css( "max-height", maxHeight );
		}
	});
	$( "#defect_photo_popup" ).on({
		popupbeforeposition: function() {
			var maxHeight = $( window ).height() - 60 + "px";
			$( "#defect_photo_popup ui-grid-b" ).css( "max-height", maxHeight );
		}
	});

	$('.div-defect-points').off('click').on('click', '.view_captured_defect', function(e){
		e.preventDefault();
		var id = $(this).attr('id');
		$('#defect_photo_block').html(photos_super_global[id]);
		$('#defect_photo_popup').popup('open').popup( 'reposition', 'positionTo: window' );
		var maxHeight = $( window ).height() - 60 + "px";
		$( "#defect_photo_popup ui-grid-b" ).css( "max-height", maxHeight );
		$('#view').page('destroy').page();
	});
	$('.link-home').off('click').on('click', function(e){
		e.preventDefault();
		location.href = 'home.html';
	});

	$('#link-change-update').off('click').on('click', function(e){
		e.preventDefault();
			//console.log(item);
			if(item.status!=0){
				return;
			}
			location.href = 'main.html#'+inspection_id;
		});

	$('#link-delete-report').off('click').on('click', function(e){
		e.preventDefault();
		if(item.status!=0){
			return;
		}
		$('#popupDialog').popup('open');
		$('#view').page('destroy').page();
	});

	$('#link-confirm-delete').off('click').on('click', function(e){
		DB.remove2('inspections',{id:inspection_id},function(err,res){
			// console.log(err);
			// console.log(res);
			location.href = 'home.html';
		});
	});

	$('#link-home').off('click').on('click', function(e){
		location.href = 'home.html';
	});

	$('#link-sign-report').off('click').on('click', function(e){
		if(item.status!=0&&item.status!=3){
			return;
		}
		location.href = 'signing.html#'+inspection_id;
	});

	$('#link-copy-report').off('click').on('click', function(e){
		location.href = 'main.html#'+inspection_id+'#copy';
	});

	var myalert = function(title, msg, callback){
		var dialog = $('#popupAlert');
		dialog.popup('open');
		dialog.find('.title').html(title);
		dialog.find('.ui-content-description').html(msg);			
		
		dialog.find('.btn_yes').off('click').on('click', callback);
	};


	$('#submit_signing').off('click').on('click', function(e){
		console.log("Submit Report");
		if(App.isOnline){
			var updateObj = {};
			updateObj['status'] = 3;
			DB.update('inspections', 'inspections.id = ' + inspection_id, updateObj , function(err, results){
				if(err.code){ console.log(err); }
				else{
						location.href = "login.html#updatedb";
				}
			});
		} else {	
			myalert('<span class="ui-btn-icon-notext ui-icon-check"></span>',localization.get('retryonline'),function(){
				document.location = "home.html";
			});
		}
	});
};
