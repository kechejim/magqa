
function showTable(t) {
	syncLog("Show Table " + t);
	DB.find(t,[],function(e,r){
		if (r) {
			var nRows = r.rows.length-1;
			if (nRows > 100) {
				nRows = 100;
				console.log("displaying first 100 rows");
			}
			for(i=0;i<nRows;i++){
				var fields = r.rows.item(i);
				console.log(fields);
			}	
		}
	});
}

function showTableStatus(t) {
	DB.find(t,[],function(e,r){
		if (r) { syncLog('  ' + r.rows.length + ' <a onclick="showTable(' + "'" + t + "')" + '">' + t + '</a>'); } else { syncLog("no " + t); }
	});
} 

App.showTables = function() {
	showTableStatus('customers');
	showTableStatus('defectcategories');
	showTableStatus('defectpoints');
	showTableStatus('factories');
	showTableStatus('firm');
	showTableStatus('inspections');
	showTableStatus('inspectors');
	showTableStatus('linkfirminspectors');
	showTableStatus('linkfirmvendors');
	showTableStatus('po');
	showTableStatus('producttypes');
}

//
//	Conditions to trigger sync
//
App.OnSyncPageShow = function() {
	try{
		if(navigator.connection.type != Connection.NONE && !_IS_RIPPLE_EMULATOR) {
			App.isOnline = true;
			$('.connectivity').removeClass('ui-icon-alert').addClass('ui-icon-check').html('<span class="str_nav_online">'+localization.strings.nav_online[Storage.get('locale')]+'</span>');
		}else{
			App.isOnline = false;
		}
	}catch(e){
		App.isOnline = false;
	}
	setTimeout(function(){
		App.DoSync();
	},1000);
}

App.OnDoUpdate = function() {
	Storage.set('version',0);
	syncLog("Exit App and launch again for forced sync");
	//App.DoSync();
}

//
//	Sync code below
//
App.DoSync = function () {
	clearSyncLog();
	if(App.isOnline) {
		App.checkServerVersion();
	} else {
		syncLog("App is offline");
		setTimeout(function(){
			if(cool(Storage.get('user-login'))){
				location.href = 'home.html';
			} else {
				location.href = "login.html";
			}
		},1000);
	}
}

App.checkServerVersion = function () {


	var localVersion = Storage.get('version');
	var ws_url = localStorage.getItem("ws_url");
	var ws_name = localStorage.getItem("ws_name");
	syncLog("Local Version : [" + localVersion + "]");	
	$.ajax({
		crossDomain: true,
		url:ws_url,
		type:'GET',
		data: {cmd:'getversion'},
		contentType: "application/json; charset=utf-8",
		success:function(download){
			syncLog("Checking Server Version on " + ws_url);
			var serverVersion = download * 1;
			syncLog("Server Version : [" + serverVersion + "]");
			Storage.set("serverVersion",serverVersion);
			if (serverVersion > localVersion ) {
				syncLog("Downloading");
				App.getTableList();
			} else {
				syncLog("No need to download");
				App.checkSubmitInspections();
			}
		},
		error:function(data){
			syncLog("Error accessing " + ws_url);
		}
	});
}

App.getTableList = function() {

	syncLog("Getting Tables List");
	var ws_url = localStorage.getItem("ws_url");
	var ws_name = localStorage.getItem("ws_name");
	$.ajax({
		crossDomain: true,
		url:ws_url,
		type:'GET',
		data: {cmd:'tablelist'},
		contentType: "application/json; charset=utf-8",
		success:function(download){
			try{
				console.log(download);
				var d = $.parseJSON(download);
				syncLog (d.length + " tables");
				Storage.set("tablelist",d);
				App.updateAllTables();
			}catch(e){
				syncLog(e);
			}
		},
		error:function(data){
			syncLog("Error getting table list form " + ws_url);
			syncLog(e);
		}
	});
}

App.updateAllTables = function() {

	console.log("updateAllTables");

	var createInpsectionsTable = function(){
		var SQL = 'CREATE TABLE IF NOT EXISTS inspections (id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,inspection_serial_id,inspection_date,inspector,email,coinspector,status,reinspect_count,po_no,inspectiontype,colorsize,colorsize_defect_points,colorsize_POMs,general_cleanliness,workflow_cleanliness,workflow_ventilation,workflow_lighting,child_labor,fabric_relaxation,broken_needle_record,totalnumworkers,totalcompletedqty,totalinspectedqty,totalrejectedqty,intime,outtime,remarks,inspection_status,factory_photos,factory_contacts_fullname,factory_contacts_title,factory_contacts_emailaddress,factory_qc_name,report_email_recipient,supervisor_email,hksupervisor_email,date_created,date_modified,inspector_sign,co_inspector_sign,qc_status,qc_remarks,factory_sign,style,fibercontent,garmentweight,shipmode,shipdate,customer,firm,vendor,factory,qty,styledesc,destination,producttype,trim, tileimage, GPSLongitude, GPSLatitude)';

		DB.execsql(SQL, function(err){
			if (typeof(err) != 'undefined') {
				syncLog(err);
			}
			processNextTable();
		});

	};

	var updateInspectionsTable = function(){
		createInpsectionsTable();
		DB.execsql('ALTER TABLE inspections ADD hksupervisor_email', function(err){});
		DB.execsql('ALTER TABLE inspections ADD colorsize', function(err){});
		DB.execsql('ALTER TABLE inspections ADD fibercontent', function(err){});
		DB.execsql('ALTER TABLE inspections ADD garmentweight', function(err){});
		DB.execsql('ALTER TABLE inspections ADD shipmode', function(err){});
		DB.execsql('ALTER TABLE inspections ADD shipdate', function(err){});
		DB.execsql('ALTER TABLE inspections ADD customer', function(err){});
		DB.execsql('ALTER TABLE inspections ADD firm', function(err){});
		DB.execsql('ALTER TABLE inspections ADD vendor', function(err){});
		DB.execsql('ALTER TABLE inspections ADD factory', function(err){});
		DB.execsql('ALTER TABLE inspections ADD styledesc', function(err){});
		DB.execsql('ALTER TABLE inspections ADD destination', function(err){});
		DB.execsql('ALTER TABLE inspections ADD producttype', function(err){});
		DB.execsql('ALTER TABLE inspections ADD email', function(err){});
		DB.execsql('ALTER TABLE inspections ADD trim', function(err){});
		DB.execsql('ALTER TABLE inspections ADD tileimage', function(err){});
		DB.execsql('ALTER TABLE inspections ADD GPSLongitude', function(err){});
		DB.execsql('ALTER TABLE inspections ADD GPSLatitude', function(err){});
	};

	var processTable = function(isDone,tablename,data){
		var items = eval('data.' + tablename + '.items');
		var fields = items[0];
		var tmp = [];
		for(k in fields){
			if(k == 'id'){
				tmp.push(k + ' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT');
			}else{
				tmp.push(k);
			}
		}
		fields = tmp.toString();
		if(items.length > 0){
			DB.newTable(tablename, fields, function(err){
				if (tablename != 'inspections') {
					syncLog("- " + tablename + " : " + items.length + " Records");
					DB.insert(tablename, items, function(err){
						if(err){
							syncLog("- " + tablename + " : " + err.message);
						}
						if (isDone) {
							syncLog("Done");
							App.setDataState();	
						} else {
							processNextTable();
						}
					});
				}
			});
		}
		else {
			syncLog("- " + tablename + " : NO RECORD");
		}
	};

	var processTableJSON = function(isDone,table,json) {
		try {
			var tableobj = JSON.parse(json);
		} catch(e) {
			syncLog("- JSON error : "+e);
			return;
		}

		if (table == 'inspections') {
			DB.check('inspections', function(err, result){
				if(result.code == 5){
					syncLog("-   Initializing inspections for new pad");
					createInpsectionsTable(isDone);
				} else {
					syncLog("-   Not overwriting Inspections table");
					updateInspectionsTable(isDone);
				}
			});
		} else {
			processTable(isDone,table,tableobj);
		}
		
	}
	
	var processNextTable = function() {
		var ws_url = localStorage.getItem("ws_url");
		var ws_name = localStorage.getItem("ws_name");
		var nextTable = queue.shift();
		var isDone = (queue.length <= 0);
		$.get(ws_url, { table: nextTable }, function( res ) {
			console.log(res);
			console.log(nextTable);
			console.log(isDone);
			processTableJSON(isDone,nextTable,res);
		});
	}

	console.log("reading queue");
	var queue = Storage.get("tablelist");
	console.log(queue);
	processNextTable();


	/*
	$(function () {
		var ws_url = localStorage.getItem("ws_url");
		var ws_name = localStorage.getItem("ws_name");	
		var chain = $.Deferred().resolve();
		Ãƒâ€šÃ‚Â 
		$.each(queue, function (i, v) {
			chain = chain.pipe(function () {
				return $.get(ws_url, { table: v })
				.pipe(
					function (res) {
						var isDone = (i == queue.length - 1);
						processTableJSON(isDone,v,res);
						//setTimeout(function(){
							//return $.Deferred().resolve();
						//},1000);
					},
					function (jqXHR, textStatus, errorThrown) {
						alert("ERROR-" + errorThrown);
						return $.Deferred().reject();
					}
				);
			});
		});
	});
	*/
};


App.setDataState = function () {
	var serverVersion = Storage.get("serverVersion");
	syncLog("Finish Updating to version " + serverVersion);
	Storage.set('version',serverVersion);
	App.checkSubmitInspections();
}

App.checkSubmitInspections = function(){
	syncLog("Checking submitted inspections for upload");


	var fp = [],dp = [],mp = [];
	var report_status = ["Accepted 接受", "Re-Inspection 重新查检", "Hold Shipment 暂停发货"];
	var inspections = [localization.get('inlineinspection'),localization.get('finalinspection')];
	var shipmode = ['Sea','Air','Truck'];
	var qc_status = ["Accepted 接受", "Rejected 不接受"];
	
	function getFactory(iid, objR){
		var obj = null;
		var total_images = 0;
		var photos = [];

		try {
			obj = $.parseJSON(unescape(objR));
			total_images = obj.photos.length;
			$.each(obj.photos, function(pk,pv){
				fp.push(pk);
				photos.push({
					id:iid+'-F'+fp.length,
					img:'data:image/png;base64,'+pv
				});
			});
			var ret = {
				"status": obj.status,
				"remarks": obj.remarks,
				"no_images": total_images,
				"images": photos
			};
			return ret;
		} catch (e) {
			defectpoints = null;
		}
	}

	function select_option(i) {
	  return $('#select-firm option[value="' + i + '"]').html();
	}

	var join = [];
	//join.push('left join po on cast(po.po_no as text) = cast(inspections.po_no as text) and cast(po.style as text) = cast(inspections.style as text) ');
	join.push('left join firm on firm.code = firm ');
	
	var select = ['inspections.*'];
	//select.push('po.fibercontent as fibercontent');
	//select.push('po.garmentweight as garmentweight');
	//select.push('po.shipmode as shipmode');
	//select.push('po.shipdate as shipdate');
	//select.push('po.vendor as vendor');
	//select.push('po.factory as factory');
	//select.push('po.qty as qty');
	//select.push('po.styledesc as styledesc');
	//select.push('po.firm as firm');
	//select.push('po.destination as destination');
	//select.push('po.producttype as producttype');
	select.push('firm.name as firmname');


	var sql = 'inspections '+join.join(' ') +' WHERE status = 3';


	DB.find(sql, select, function(err, results) {

		if(typeof(results) != 'undefined' && results.rows){

			var reports = [];
			if(results.rows.length>0){

				syncLog("Processing " + results.rows.length + " reports");
				for(i=0;i<=results.rows.length-1;i++){


					syncLog("- Defect Points");
					var items = results.rows.item(i);
					
					var defectpoints;
					try {
						//start defect points
						defectpoints = $.parseJSON(unescape(items.colorsize_defect_points));
					} catch (e) {
						defectpoints = null;
					}
					var objDP = [];

					syncLog("- Parsing Defect Points");

					if (defectpoints != null) {
						console.log(defectpoints);
						$.each(defectpoints, function(key,val){
							var points = [];								
							$.each(val, function(a,b){
								if(!check(b)){
									$.each(b.defectpoints, function(dk,dv){
										var total_images = 0;
										var photos = [];
										try{
											total_images = dv.photos.length;
											$.each(dv.photos, function(pk,pv){
												dp.push(pk);
												var photo = pv;
												if (pv.indexOf('data') != 0) {
													photo = 'data:image/png;base64,'+pv
												}
												photos.push({
													id:items.inspection_serial_id+'-D'+(dp.length),
													img:photo
												});
											});
										}catch(e){
											console.log("Push image error");
											console.log(e);
										}
										//console.log ("-  "+ dv.description);
										points.push({
											"subcatid":dv.subcatid,
											"seqno":dv.seqno,
											"defect":dv.description,
											"defect_qty_minor":dv.minor_qty,
											"defect_qty_major":dv.major_qty,
											"remarks_minor":dv.minor_remarks,
											"remarks_major":dv.major_remarks,
											"no_images": total_images,
											"images": photos
										});
									});					
								}
							});
							var json_dp = {
								"color_size":val.colorsize,
								"po_qty":val.quantity,
								"completed_qty":val.completed_quantity,
								"inspected_qty":val.inspected_quantity,
								"rejected_qty_major":val.rejected_quantity_major,
								"rejected_qty_minor":val.rejected_quantity_minor,
								"details":points
							};
							objDP.push(json_dp);
							
						});
					} else {
						syncLog("-   No defect points");
					}
					

					syncLog("- Parsing Measurement Photos");
					// end defect points
					var measurement_photos 
					var total_images = 0;
					var photos = [];

					try {
						measurement_photos = $.parseJSON(unescape(items.factory_photos));
					} catch (e) {
						measurement_photos = null;
					}
					
					if (measurement_photos != null) {
						total_images = measurement_photos.length;
						$.each(measurement_photos, function(pk,pv){
							mp.push(pk);
							var photo = pv;
							if (pv.indexOf('data') != 0) {
								photo = 'data:image/png;base64,'+pv
							}
							photos.push({
								id:items.inspection_serial_id+'-M'+(mp.length),
								img:photo
							});
						});
					} else {
						syncLog("-   No measurement photos");
					}
					syncLog("- Parsing Factory Conditions");
					var general_cleanliness = getFactory(items.inspection_serial_id, items.general_cleanliness);
					var workflow_cleanliness = getFactory(items.inspection_serial_id, items.workflow_cleanliness);
					var workflow_lighting = getFactory(items.inspection_serial_id, items.workflow_lighting);
					var workflow_ventilation = getFactory(items.inspection_serial_id, items.workflow_ventilation);
					var child_labor = getFactory(items.inspection_serial_id, items.child_labor);
					var fabric_relaxation = getFactory(items.inspection_serial_id, items.fabric_relaxation);
					var broken_needle_record = getFactory(items.inspection_serial_id, items.broken_needle_record);
					
					syncLog("- Other Fields");

					try {

					var trim;
					try {
						trim = $.parseJSON(unescape(items.trim));
					} catch (e) {
						trim = null;
					}

					reports.push({
						"firm":items.firm,
						"firmname":items.firmname,
						"inspection_no":items.inspection_serial_id,
						"inspector":items.inspector,
						"email":items.email,
						"inspection":inspections[items.inspectiontype-1],// 
						"reinspect_no":items.reinspect_count, 
						"coinspector":items.coinspector,
						"qc_comment":qc_status[items.qc_status-1], 
						"qc_remarks":items.qc_remarks,
						"po_no":items.po_no,
						"date":items.inspection_date,
						"customer":items.customer,
						"companyname":items.companyname,
						"in_time":items.intime,
						"out_time":items.outtime,
						"factory_in_charge_name":items.factory_contacts_fullname,
						"factory_in_charge_posiion":items.factory_contacts_title,
						"factory_qc_name":items.factory_qc_name,
						"report_email_recipient" : items.report_email_recipient,
						"factory_contacts_emailaddress" : items.factory_contacts_emailaddress,
						"supervisor_email" : items.supervisor_email, // 
						"hksupervisor_email" : items.hksupervisor_email, // 
						"status":report_status[items.inspection_status-1],
						"inspection_date":items.inspection_date,
						"factory":items.factory,
						"producttype":items.producttype,
						"vendor":items.vendor,
						"style":items.style,
						"styledesc":items.styledesc,
						"qty":items.qty,
						"shipdate":items.shipdate,
						"shipmode":items.shipmode,
						"destination":items.destination,
						"fiber_content":items.fibercontent,
						"garment_weigh":items.garmentweight,
						"defect":objDP,
						"general_cleanliness":general_cleanliness,
						"workfloor_cleanliness":workflow_cleanliness,
						"workfloor_lighting":workflow_lighting,
						"workfloor_ventilation":workflow_ventilation,
						"child_labor_awareness":child_labor,
						"fabric_relaxation_record":fabric_relaxation,
						"broken_needle_record":broken_needle_record,
						"measurement_photo_qty":total_images,
						"measurements_photos" : photos,//
						"no_total_workers":items.totalnumworkers,
						"fullname":items.factory_contacts_fullname,
						"title":items.factory_contacts_title,
						"inspector_sign":items.inspector_sign,
						"factory_sign":items.factory_sign,								
						"total_completed_qty":items.totalcompletedqty,//
						"total_inspected_qty":items.totalinspectedqty,//
						"total_rejected_qty_minor":items['totalrejectedqty-minor'],
						"total_rejected_qty_major":items['totalrejectedqty-major'],
						"trim":trim,
						"tileimage":items['tileimage'],
						"GPSLongitude": items['GPSLongitude'],
						"GPSLatitude":items['GPSLatitude'],
						"date_created":items['date_created'],
						"date_modified":items['date_modified']
					});
					} catch (e) {
						alert(e);
					}

				}

				App.ajaxSubmitReport(items.id,reports);
			} else {
				syncLog("Nothing to upload");
				App.finishSubmitReport();
			}
		} else {
			syncLog("Nothing to upload");
			App.finishSubmitReport();
		}
	});
};

App.finishSubmitReport= function(){
	syncLog("All Done");
	setTimeout(function(){
		if(cool(Storage.get('user-login'))){
			location.href = 'home.html';
		} else {
			location.href = "login.html";
		}
	},3000);
}

App.ajaxSubmitReport = function(inspection_id,json){
	var json = [
		{"report":{"items": json}}
	];

	syncLog("Stringify JSON");

	var strJSON = JSON.stringify(json);
	var ws_url = localStorage.getItem("ws_url");
	var ws_name = localStorage.getItem("ws_name");
	var reportLength = strJSON.length;
	var reportLengthUnit = " bytes";
	if (reportLength > 1024*1024) {
		reportLength = Math.ceil(reportLength / 1024 / 1024);
		reportLengthUnit = " MB";
	} else if (reportLength > 1024) {
		reportLength = Math.ceil(reportLength / 1024);
		reportLengthUnit = " KB";
	}

	syncLog("Posting Report, size = "+reportLength + reportLengthUnit);
//	var updateObj = {};
//	updateObj['status'] = 0;			
//	console.log("Inpsection ID " + inspection_id + " submitted");							
//	DB.update('inspections', 'inspections.id = ' + inspection_id, updateObj , function(err, results){
//		if(err.code){ console.log(err); }
//		else{
//			App.finishSubmitReport();
//		}
//	});
//	return;
	$.ajax({
		crossDomain: true,
		url:ws_url,
		type:'PUT',
		data:strJSON, // <-- json data to be sent to web service.
		contentType: "application/json; charset=utf-8",
		success:function(data){
			var updateObj = {};
			updateObj['status'] = 2;			
			console.log("Inpsection ID " + inspection_id + " submitted");							
			DB.update('inspections', 'inspections.id = ' + inspection_id, updateObj , function(err, results){
				if(err.code){ console.log(err); }
				else{
					App.finishSubmitReport();
				}
			});
		},
		error:function(data){
			syncLog('Online Transaction Fail');
			syncLog(data);
		}
	});
}